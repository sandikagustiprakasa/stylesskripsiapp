import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { setCurrentOrientation , orientationCleanup} from 'nativescript-screen-orientation';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "tns-core-modules/ui/page"
import { View, Color } from "tns-core-modules/ui/core/view"
import { ScrollView, ScrollEventData } from "tns-core-modules/ui/scroll-view";
import { Animation, AnimationDefinition, CubicBezierAnimationCurve } from "tns-core-modules/ui/animation";
import { SwipeGestureEventData, SwipeDirection, PanGestureEventData, GestureStateTypes } from "tns-core-modules/ui/gestures/gestures";

import * as Dialog from 'ui/dialogs';

import { EventData } from "tns-core-modules/data/observable";
import { isAndroid } from "tns-core-modules/platform";

import { registerElement } from "nativescript-angular/element-registry";
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

import { ServiceProxy } from '../../../../shared/service-proxy.service';
import { GlobalParamCustomer } from '../../../../shared/service-proxy.service';
import { GlobalParamPrinterSetting } from '../../../../shared/service-proxy.service';
import { CustomerTransactionHistoryService } from '../../../../shared/service-proxy.service';



import { Hprt, HPRTPrinter } from "nativescript-hprt";
import * as $ from 'jquery';
const AratakaConfig = require("~/assets/AratakaConfig.js").AratakaConfig;

@Component({
  selector: 'app-admin-customer-trans-history',
  templateUrl: './admin-customer-trans-history.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-customer-trans-history.component.css'
  ]
})
export class AdminCustomerTransHistoryComponent implements OnInit {
  public APIURL = this._SP.APIURL;

  private hprt: Hprt;
  public isBluetoothEnabled : boolean = false;
    
  //Request CustomerTransHistory List
  public ListRequest = {
    isHistory : true,
    Top : 5,
    Page: 1,
    PerPage: 5,
    CustomerTransactionHistory_ID : null,
    MasterCustomerData_ID : null,
    MasterMerchant_ID : null,
    MasterSite_ID : null,
    MasterProject_ID: this._SP.ProjectID,
    IsConfirmation : 1,
    IsActive : 1
  }

  //CustomerTransHistory List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion search CustomerTransHistory List
  public Subscribtion = null;

  //information display for CustomerTransHistory List
  public isCustomerTransHistoryListLoading = false
  public isCustomerTransHistoryListEmpty = true
  public isPullRefresh = null;
  public isLoadMoreItem = false;
  public LoadMoreItemMsg = "";


  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamCustomer : GlobalParamCustomer,
    private _GlobalParamPrinterSetting : GlobalParamPrinterSetting,
    private _CustomerTransactionHistoryService : CustomerTransactionHistoryService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);

     this.hprt = new Hprt()
     this.isBluetoothEnabled = this.hprt.isBluetoothEnabled();

  }

  ngOnInit() {


    this.ListRequest.MasterCustomerData_ID = this._GlobalParamCustomer.CustomerID
    this.GettingListCustomerTransHistoryDataAPI(this.ListRequest,false)

    setCurrentOrientation("portrait",function(){
			console.log("portrait orientation");
    });

    this.isLoadMoreItem = false
    this.LoadMoreItemMsg = ""

    this._SP.setUrlBefore('/admin/customer-detail')
   
  }

  //Mobile Code ------------------------------------------------------------------------------------------------
  
  public wrapperLayout : View = undefined;
  public scrollview : ScrollView = undefined;
  public bar : View = undefined;

  public minStrecth = 0;
  public maxStrecth = 40;
  public animationDuration = 250;
  public animation : Animation = undefined;
  public prevTranslateY = 0;
  public prevScale = 0;

  onWrapperLoaded(args : EventData){


    
    // this.wrapperLayout = args.object as View;
    // const page = this.wrapperLayout.page;

    // this.scrollview = page.getViewById("scrollview") as ScrollView;
    // this.bar = page.getViewById("bar") as View;

    // this.bar.eachChildView((v : View) =>{
    //   v.opacity = 0;
    //   return true;
    // });

    // this.bar.translateY = this.bar.translateY - this.maxStrecth;
    // this.bar.height = this.maxStrecth;

    // this.scrollview.marginTop = -1 * this.maxStrecth

    // this.scrollview.on('pan', (args : PanGestureEventData) => {

    //   if(args.state == GestureStateTypes.began){

    //     if(this.animation && this.animation.isPlaying){
    //       this.animation.cancel();
    //     }
      
    //   }else if(args.state == GestureStateTypes.changed){

        
    //     if(args.deltaY < 0){
    //       let newY = this.scrollview.translateY + args.deltaY - this.prevTranslateY;
    //       this.scrollview.translateY = newY;
    //       this.prevTranslateY = args.deltaY;

    //       this.prevScale = this.getScale(this.maxStrecth, newY);
    //       this.bar.eachChildView((v : View) => {
    //         v.opacity = this.prevScale
    //         return true
    //       })

    //       if(newY <= this.maxStrecth && newY >= this.minStrecth){
    //         this.bar.translateY = newY - this.maxStrecth;
    //       }

    //     }

    //   }else if(args.state == GestureStateTypes.ended){
    //     if(args.deltaY < 0){
    //       let transY = this.maxStrecth;
    //       if(args.deltaY < this.maxStrecth / 2){
    //         transY = this.minStrecth
    //       }

    //       const def1 : AnimationDefinition = {
    //         target : this.scrollview,
    //         duration : this.animationDuration,
    //         translate : { x : 0, y : transY},
    //         curve : new CubicBezierAnimationCurve(.18, .52, 0, 1)
    //       }

    //       const def2 : AnimationDefinition = {
    //         target : this.bar,
    //         duration : this.animationDuration,
    //         translate : { x : 0, y : transY - this.maxStrecth},
    //         curve : new CubicBezierAnimationCurve(.18, .52, 0, 1)
    //       }

          
    //       const childDefs : AnimationDefinition[] = [];
    //       this.bar.eachChildView((v : View) => {

    //         const def3 : AnimationDefinition = {
    //           target : v,
    //           duration : this.animationDuration,
    //           opacity : transY == this.maxStrecth? 1 : 0
    //         }
            
    //         childDefs.push(def3);
    //         return true;
    //       })

    //       this.animation = new Animation([def1,def2, ...childDefs]);
    //       this.animation.play();
    //     }
    //   }

    // })

    // this.scrollview.on('swipe', (args : SwipeGestureEventData) => {

    //   if(this.animation && this.animation.isPlaying){
    //     this.animation.cancel();
    //   }

    //   if(args.direction == SwipeDirection.down){

    //     const def1 : AnimationDefinition = {
    //       target : this.bar,
    //       duration : this.animationDuration,
    //       translate : { x : 0, y : this.minStrecth},
    //       curve : new CubicBezierAnimationCurve(.18, .52, 0, 1)
    //     }

    //     const def2 : AnimationDefinition = {
    //       target : this.scrollview,
    //       duration : this.animationDuration,
    //       translate : { x : 0, y : this.maxStrecth},
    //       curve : new CubicBezierAnimationCurve(.18, .52, 0, 1)
    //     }

    //     const childDefs : AnimationDefinition[] = [];
    //     this.bar.eachChildView((v : View) => {

    //       const def3 : AnimationDefinition = {
    //         target : v,
    //         duration : this.animationDuration * 2,
    //         opacity : 1
    //       }
          
    //       childDefs.push(def3);
    //       return true;
    //     })

    //     this.animation = new Animation([def1,def2, ...childDefs]);
    //     this.animation.play();
    //   }

    // })
  }

  getScale(fullDen : number, partNum : number){
    return partNum / fullDen;
  }


  //Take By User------------------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/customer-detail'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  BackAfterListEmpty(){
    this.GettingListCustomerTransHistoryDataAPI(this.ListRequest,false)
  }

  refreshList(args) {
    console.log("Refresh 1")
    this.isPullRefresh = args.object;

    this.ListRequest.Page = 1
    this.ListRequest.PerPage = 10   
    
    this.GettingListCustomerTransHistoryDataAPI(this.ListRequest,true)
    // setTimeout(function () {
    //   this.isPullRefresh .refreshing = false;
    //   console.log("Refresh 2")
    // }, 1000);
  }

  loadMoreItems(args: ScrollEventData) {
    const scrollView = args.object as ScrollView;

    // console.log("Height: " + scrollView.scrollableHeight);
    // console.log("scrollX: " + args.scrollX);
    // console.log("scrollY: " + args.scrollY);
    
    if(scrollView.scrollableHeight == args.scrollY){
      if(this.ListRequest.Page < Math.ceil(this.ListDataPaging.count / this.ListRequest.PerPage)){
        this.ListRequest.Page = this.ListRequest.Page + 1 
        this.GettingListCustomerTransHistoryDataLoadMoreItemAPI(this.ListRequest)
      }
    }
  }

  PrintReceipt(_i){
    if(!this.isBluetoothEnabled){
      this.GoToPrintterSetting()
    }else{

      if(this._GlobalParamPrinterSetting.GetIsSelectedPrinter){
        this.PrintReceiptData(this.ListData[_i])
      }else{
        this.GoToPrintterSetting()
      }

    }

  }

  GoToPrintterSetting(){
    this._GlobalParamPrinterSetting.SetActionFrom("app-admin-customer-trans-history")
    this.routerExtensions.navigate(['/admin/printersetting'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }


  //Funtion Engine to API---------------------------------------------------------------------------------------
  GettingListCustomerTransHistoryDataAPI(_Request,_isPull){
    this.ListData = []
    this.isCustomerTransHistoryListLoading = true
    this.isCustomerTransHistoryListEmpty = false
    this.Subscribtion = this._CustomerTransactionHistoryService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          //console.log(JSON.stringify(result))

          if(result.Data != null){
            
            this.ListData = result.Data

            this.ListDataPaging.count = result.TotalData

          }else{
            this.ListData = []
            this.isCustomerTransHistoryListEmpty = true
          }
        }else{
          this.ListData = []
          this.isCustomerTransHistoryListEmpty = true
        }
        
        this.isCustomerTransHistoryListLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
					title : "OH!",
					message : JSON.stringify(err.error),
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
        })
        
        this.isCustomerTransHistoryListLoading = false
        this.isCustomerTransHistoryListEmpty = true

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
          console.log("Refresh 2")
        }

        this._cRef.detectChanges();
      });
  }

  GettingListCustomerTransHistoryDataLoadMoreItemAPI(_Request){
    this.isLoadMoreItem = true
    this.LoadMoreItemMsg = "Load Data.."
    this.Subscribtion = this._CustomerTransactionHistoryService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          
          if(result.Data != null){
            //console.log("More Item", JSON.stringify(result))
            this.ListData = this.ListData.concat(result.Data)

          }else{
            this.LoadMoreItemMsg = "You have reach all data"
          }
        }else{
          this.LoadMoreItemMsg = "There is some error, pleas refresh and try again"
        }
        
        this.isLoadMoreItem = false
        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
					title : "OH!",
					message : "Load More item : " + JSON.stringify(err.error),
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
        })

        this.LoadMoreItemMsg = "Load More item : " + JSON.stringify(err.error)
        this.isLoadMoreItem = false

        this._cRef.detectChanges();
      });
  }

  PrintReceiptData(_data) {
    // Sum this numbers to get attribute combination
    // Example: 
    // Double height: 16
    // Double Width: 32
    // Underline: 4
    // Bold: 2
    // Mini: 1
    // White text: 8

    var dateNow = new Date();

    this.hprt.newLine();
    this.hprt.printTextCenter("LIPPO MALLS INDONESIA");
    this.hprt.printTextCenter("----------");
    this.hprt.newLine();
    this.hprt.printTextCenter("Jl. Boulevard Gajah Mada No.2121, RT.001/RW.009, Panunggangan Bar., Kec. Cibodas, Kota Tangerang, Banten 15810");
    this.hprt.printTextCenter("Kota Tangerang, Indonesia");
    this.hprt.printTextCenter("Twitter: @lippomalls");
    this.hprt.newLine();
    this.hprt.printTextSimple("Date : "+ AratakaConfig.formatDateYMDHS(dateNow));
    this.hprt.newLine();
    
    if(_data.MasterRedeem_ID != null){
      this.hprt.printTextMini("Item : "+ _data.MasterRedeem_Name);    
      this.hprt.horizontalLine();       
    }
    if(_data.MasterMerchant_ID != null){
      this.hprt.printTextMini("Merchant : "+ _data.MasterMerchant_Name);    
      this.hprt.horizontalLine();       
    }
    this.hprt.printTextMini("Earning Type : "+ _data.MasterEarningType_Name);    
    this.hprt.horizontalLine();       
    this.hprt.printTextMini("Card Type : "+ _data.CustomerDataMemberCardType_Name); 
    this.hprt.horizontalLine();       
    this.hprt.printTextMini( "Amount : Rp. " + AratakaConfig.formatRupiah(_data.CustomerTransactionHistory_Amount).toString());    
    this.hprt.horizontalLine();     
    this.hprt.printTextMini( "Mall : "+ _data.MasterSite_Name);    
    this.hprt.horizontalLine();    
    if(_data.CustomerTransactionHistory_LuckyDrawPoint != null){
      this.hprt.printTextMini("L Draw Point : "+ _data.CustomerTransactionHistory_LuckyDrawPoint.toString());    
      this.hprt.horizontalLine();       
    }    
    this.hprt.printTextMini( "Redeem Point : "+ _data.CustomerTransactionHistory_RedeemPoint.toString());    
    this.hprt.horizontalLine();
    if(_data.MasterTransactionType_ID != null){
      this.hprt.printTextMini( "Transaction : "+ _data.MasterTransactionType_Name);    
      this.hprt.horizontalLine();       
    }    
    if(_data.MasterBank_ID != null){
      this.hprt.printTextMini( "Bank : "+ _data.MasterBank_Name);    
      this.hprt.horizontalLine();       
    }    
    if(_data.MasterEWallet_ID != null){
      this.hprt.printTextMini( "E-Wallet : "+ _data.MasterEWallet_Name);    
      this.hprt.horizontalLine();       
    }    
    // for (let index = 0; index < cart.length; index++) {
    //     this.hprt.printTextSimple(cart[index]);    
    //     this.hprt.horizontalLine();        
    // }
    this.hprt.newLine(4);
  }

}
