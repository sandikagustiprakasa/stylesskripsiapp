import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';

import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { AuthRoutingModule } from "./auth-routing.module";
import { AuthSigninComponent } from './auth-signin/auth-signin.component';
import { AuthForgotpasswordComponent } from './auth-forgotpassword/auth-forgotpassword.component';



@NgModule({
  declarations: [AuthSigninComponent, AuthForgotpasswordComponent],
  imports: [
    NativeScriptCommonModule,
    AuthRoutingModule,
    NativeScriptFormsModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AuthModule { }
