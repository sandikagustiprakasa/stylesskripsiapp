import { Component, OnInit } from '@angular/core';
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import * as ApplicationSettings from "application-settings";

import { Page } from "ui/page"
import * as application from "tns-core-modules/application";

import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-mall-detail',
  templateUrl: './admin-mall-detail.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-mall-detail.component.css'
  ]
})
export class AdminMallDetailComponent implements OnInit {

  public APIURL = this._SP.APIURL;

  public DetailData = {}

  constructor(
    private page: Page,
    private params: ModalDialogParams,
    private _SP : ServiceProxy,) { }

  ngOnInit() {
    this.DetailData = this.params.context.Detail
  }

  CloseClick(){
    this.params.closeCallback()
  }

}
