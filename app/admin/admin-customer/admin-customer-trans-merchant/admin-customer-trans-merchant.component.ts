import { Component, OnInit, ViewContainerRef, ChangeDetectorRef, ViewChildren, ElementRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import { TextField } from "tns-core-modules/ui/text-field"
import * as Dialog from 'ui/dialogs';

import { View, Color } from "tns-core-modules/ui/core/view"
import { EventData } from "tns-core-modules/data/observable";

import { GlobalParamTransaction } from '../../../../shared/service-proxy.service';
import { MerchantService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-customer-trans-merchant',
  templateUrl: './admin-customer-trans-merchant.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-customer-trans-merchant.component.css'
  ]
})
export class AdminCustomerTransMerchantComponent implements OnInit {

  //Filter model
  public keyword : string = ""

  
  //RequestMerchant List
  public ListRequest = {
    Page: 1,
    PerPage: 20,
    Search : "",
    MasterMerchant_ID: null,
    MasterSite_ID : [],
    MasterStoreCategory_ID : null,
    MasterBrand_ID : null,
    MasterProject_ID: this._SP.ProjectID
  }

  //Merchant List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchMerchant List
  public Subscribtion = null;

  //information display forMerchant List
  public isMerchantListLoading = false
  public isMerchantListEmpty = true
  public isPullRefresh = null;
  public isLoadMoreItem = false;
  public LoadMoreItemMsg = "";

  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamTransaction : GlobalParamTransaction,
    private _MerchantService : MerchantService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
        
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

    }, 500);
  }

  ngOnInit() {
    this._GlobalParamTransaction.SetSite({
      ID : (this._SP.UserSiteData.ID != null)? [this._SP.UserSiteData.ID] : [],
      Name :(this._SP.UserSiteData.Name != null)? [this._SP.UserSiteData.Name] : null,
    })
    this.ListRequest.MasterSite_ID = this._GlobalParamTransaction.GetSite.ID
    this.GettingListMerchantDataAPI(this.ListRequest,false)

    this.isLoadMoreItem = false
    this.LoadMoreItemMsg = ""

    this._SP.setUrlBefore('/admin/customer-transaction')
  }

  //Mobile Code ------------------------------------------------------------------------------------------------
  public wrapperLayout : View = undefined;
  public searchBar : TextField = undefined;


  onWrapperLoaded(args : EventData){
        
    this.wrapperLayout = args.object as View;
    const page = this.wrapperLayout.page;

    this.searchBar = page.getViewById("searchBar") as TextField;

    //this.searchBar.focus();
  }

  //Take By User------------------------------------------------------------------------------------------------
  refreshList(args) {
    console.log("Refresh 1")
    this.isPullRefresh = args.object;

    this.ListRequest.Page = 1
    this.ListRequest.PerPage = 20   
    
    this.GettingListMerchantDataAPI(this.ListRequest,true)
    // setTimeout(function () {
    //   this.isPullRefresh .refreshing = false;
    //   console.log("Refresh 2")
    // }, 1000);
  }

  loadMoreItems() {
    if(this.ListRequest.Page < Math.ceil(this.ListDataPaging.count / this.ListRequest.PerPage)){
      this.ListRequest.Page = this.ListRequest.Page + 1 
      this.GettingListMerchantDataLoadMoreItemAPI(this.ListRequest)
    }
  }

  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/customer-transaction'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  onItemTap(args: ItemEventData){
    //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);
    this._GlobalParamTransaction.SetMerchant({
      ID : this.ListData[args.index].MasterMerchant_ID,
      Name : this.ListData[args.index].MasterMerchant_Name,
      StoreCategoryID : this.ListData[args.index].MasterStoreCategory_ID,
      StoreCategoryName : this.ListData[args.index].MasterStoreCategory_Name
    })

    this.routerExtensions.navigate(['/admin/customer-transaction'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }


  KeywordReturnPress(){
    this.ListRequest.Search = this.keyword      
    this.DoSearch()
  }

  KeywordChange(){
    
    this.ListRequest.Search = this.keyword      
    this.DoSearch()

  }

  DoSearch(){
      
    this.ListRequest.Page = 1
      if(this.Subscribtion){
      this.Subscribtion.unsubscribe()
    }
    this.GettingListMerchantDataAPI(this.ListRequest,false)    

  }

  BackAfterListEmpty(){
    this.keyword = ""
    this.ListRequest.Search = this.keyword    
    this.GettingListMerchantDataAPI(this.ListRequest,false)
  }


  //Funtion Engine to API---------------------------------------------------------------------------------------
  GettingListMerchantDataAPI(_Request,_isPull){
    this.ListData = []
    this.isMerchantListLoading = true
    this.isMerchantListEmpty = false
    this.Subscribtion = this._MerchantService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          //console.log(JSON.stringify(result))

          if(result.Data != null){
            
            this.ListData = result.Data
            this.ListDataPaging.count = result.TotalData

          }else{
            this.ListData = []
            this.isMerchantListEmpty = true
          }
        }else{
          this.ListData = []
          this.isMerchantListEmpty = true
        }
        
        this.isMerchantListLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
          title : "OH!",
          message : JSON.stringify(err.error),
          okButtonText : 'Okay'
        }
        Dialog.alert(options).then(()=>{
        })
        
        this.isMerchantListLoading = false
        this.isMerchantListEmpty = true

        this._cRef.detectChanges();
      });
  }

  GettingListMerchantDataLoadMoreItemAPI(_Request){
    this.isLoadMoreItem = true
    this.LoadMoreItemMsg = "Load Data.."
    this.Subscribtion = this._MerchantService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          
          if(result.Data != null){
            console.log("More Item", JSON.stringify(result))
            this.ListData = this.ListData.concat(result.Data)

          }else{
            this.LoadMoreItemMsg = "You have reach all data"
          }
        }else{
          this.LoadMoreItemMsg = "There is some error, pleas refresh and try again"
        }
        
        this.isLoadMoreItem = false
        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
					title : "OH!",
					message : "Load More item : " + JSON.stringify(err.error),
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
        })

        this.LoadMoreItemMsg = "Load More item : " + JSON.stringify(err.error)
        this.isLoadMoreItem = false

        this._cRef.detectChanges();
      });
  }

}
