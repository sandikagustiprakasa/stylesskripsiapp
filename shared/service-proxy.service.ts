import { Injectable, Inject, Injector, Optional, InjectionToken } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Image } from "tns-core-modules/ui/image";

import * as ApplicationSettings from "application-settings";

const ProjectID = 1;

const API_URL_Example = "https://picdevapi.vnetcloud.com/STYLES/";
const API_URL = "http://192.168.43.168:8081/";


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private injector: Injector, private http : HttpClient) { }

  Token(_data) {
    let data = _data;
    let headers = new HttpHeaders({
      'Accept': 'application/json', 
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'token', data,options);

  }

  GetUserData(_data) {
    
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterUserData/SignIn', data, options);
  }

  SignIn(_UserLoginData) {
    let data = _UserLoginData; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json', 
      //'Postman-Token': 'd819ccd8-a9ef-46df-bafd-2cc60e2ee5f2', 
      //'cache-control': 'no-cache'  
    });
    let options = { headers: headers };

    //console.log(JSON.stringify(_UserLoginData))
    return this.http.post(API_URL +'api/login/do', data, options);
  }

  UpdatePassword(_data) {
    
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      //'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterUserData/UpdatePassword', data, options);
  }

  ForgotPassword(_data) {
    
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      //'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterUserData/ForgotPassword', data, options);
  }


}

@Injectable({
  providedIn: 'root'
})
export class SiteService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterSite/List', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class ProvinceService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterProvince/List', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterBrand/List', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class StoreCategoryService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterStoreCategory/List', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class MerchantService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterMerchant/List', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class CardTypeService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterMemberCardType/List', data, options);
  }

}


@Injectable({
  providedIn: 'root'
})
export class FormulaService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterFormulaPoint/List', data, options);
  }

  Detail(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterFormulaPoint/Detail', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class PointTypeService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterPointType/List', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class RedeemService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterRedeem/List', data, options);
  }

  List_v2(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterRedeem/List-V2', data, options);
  }

  Detail(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterRedeem/Detail', data, options);
  }


  CustomerRedeem(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterRedeem/CustomerRedeem', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class RedeemTypeService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterRedeemType/List', data, options);
  }

}


@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterCustomerData/List', data, options);
  }

  Detail(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterCustomerData/Detail', data, options);
  }

}

@Injectable({
  providedIn: 'root'
})
export class CustomerTransactionHistoryService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'CustomerTransactionHistory/List', data, options);
  }

  CustomerPoint(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'CustomerTransactionHistory/CustomerPoint', data, options);
  }

  Insert(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'CustomerTransactionHistory/Insert-v2', data, options);
  }

  StrukUpdate(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'CustomerTransactionHistory/UpdateStruk', data, options);
  }

  StrukUpdateMultipart(_data) {
    
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };

    return this.http.post<any>(API_URL +'CustomerTransactionHistory/UpdateStrukMultiPart', data, options);
  }


}


@Injectable({
  providedIn: 'root'
})
export class BankService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterBank/List', data, options);
  }


}


@Injectable({
  providedIn: 'root'
})
export class EWalletService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'MasterEWallet/List', data, options);
  }


}


@Injectable({
  providedIn: 'root'
})
export class CustomerDataMemberCardTypeService {

  constructor(private injector: Injector, private http : HttpClient) { }

  List(_data) { 
    const service = this.injector.get(ServiceProxy)
    let data = _data; 
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : service.getUserTokenBearer.token_type + ' '+  service.getUserTokenBearer.access_token
    });
    let options = { headers: headers };
    return this.http.post(API_URL +'CustomerDataMemberCardType/List', data, options);
  }


}

//-----------------------------------------------------------------------------------------------

@Injectable({
  providedIn: 'root'
})
export class GlobalParamSite{

  
  public Search = ""
  public Province = {
    ID : null,
    Name : null,
  }

  SetSearch(_value){
    this.Search = _value; 
  }

  get GetSearch() {
    return this.Search
  }

  SetProvince(_value) {
    this.Province = _value;
  }

  get GetProvince() {
    return this.Province
  }

  constructor() { }
}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamBrand{

  
  public Search = ""
  public StoreCategory = {
    ID : null,
    Name : null,
  }

  SetSearch(_value){
    this.Search = _value; 
  }

  get GetSearch() {
    return this.Search
  }

  SetStoreCategory(_value) {
    this.StoreCategory = _value;
  }

  get GetStoreCategory() {
    return this.StoreCategory
  }

  constructor() { }
}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamMerchant{

  public service = this.injector.get(ServiceProxy)

  public Search = ""
  public StoreCategory = {
    ID : null,
    Name : null,
  }

  public Brand = {
    ID : null,
    Name : null,
  }

  public Site = {
    ID : (this.service.UserSiteData.ID != null)? [this.service.UserSiteData.ID] : [],
    Name : (this.service.UserSiteData.Name != null)? [this.service.UserSiteData.Name] : null,
  }

  SetSearch(_value){
    this.Search = _value; 
  }

  get GetSearch() {
    return this.Search
  }

  SetStoreCategory(_value) {
    this.StoreCategory = _value;
  }

  get GetStoreCategory() {
    return this.StoreCategory
  }

  SetBrand(_value) {
    this.Brand = _value;
  }
  
  get GetBrand() {
    return this.Brand
  }

  SetSite(_value) {
    this.Site = _value;
  }
  
  get GetSite() {
    return this.Site
  }

  constructor(private injector: Injector) { }

}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamCardType{

  public service = this.injector.get(ServiceProxy)

  public Search = ""

  SetSearch(_value){
    this.Search = _value; 
  }

  get GetSearch() {
    return this.Search
  }


  constructor(private injector: Injector, private http : HttpClient) { }

}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamFormula{

  public service = this.injector.get(ServiceProxy)

  public Search = ""
  public PointType = {
    ID : null,
    Name : null,
  }
  public IsDefault = null
  public IsSpecial = null
  public IsActive = true

  public FormulaID = null

  SetSearch(_value){
    this.Search = _value; 
  }

  get GetSearch() {
    return this.Search
  }

  SetPointType(_value) {
    this.PointType = _value;
  }

  get GetPointType() {
    return this.PointType
  }

  SetIsDefault(_value) {
    this.IsDefault = _value;
  }

  get GetIsDefault() {
    return this.IsDefault
  }

  SetIsSpecial(_value) {
    this.IsSpecial = _value;
  }

  get GetIsSpecial() {
    return this.IsSpecial
  }

  SetIsActive(_value) {
    this.IsActive = _value;
  }

  get GetIsActive() {
    return this.IsActive
  }


  SetFormulaID(_value) {
    this.FormulaID = _value;
  }

  get GetFormulaID() {
    return this.FormulaID
  }

 

  constructor(private injector: Injector, private http : HttpClient) { }

}


@Injectable({
  providedIn: 'root'
})
export class GlobalParamRedeem{

  
  public Search = ""
  public StoreCategory = {
    ID : null,
    Name : null,
  }

  public IsActive = true

  public RedeemID = null

  SetSearch(_value){
    this.Search = _value; 
  }

  get GetSearch() {
    return this.Search
  }

  SetStoreCategory(_value) {
    this.StoreCategory = _value;
  }

  get GetStoreCategory() {
    return this.StoreCategory
  }

  SetIsActive(_value) {
    this.IsActive = _value;
  }

  get GetIsActive() {
    return this.IsActive
  }

  
  SetRedeemID(_value) {
    this.RedeemID = _value;
  }

  get GetRedeemID() {
    return this.RedeemID
  }
  
  constructor() { }
}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamCustomer{

  
  public Search = ""
  public CustomerID = null;
  public MembershipCardTypeID = null;

  public CustomerTotalPoint = null;


  SetSearch(_value){
    this.Search = _value; 
  }

  get GetSearch() {
    return this.Search
  }


  SetCustomerID(_value){
    this.CustomerID = _value; 
  }

  get GetCustomerID() {
    return this.CustomerID
  }

  SetMembershipCardTypeID(_value){
    this.MembershipCardTypeID = _value; 
  }

  get GetMembershipCardTypeID() {
    return this.MembershipCardTypeID
  }

  SetCustomerTotalPoint(_value){
    this.CustomerTotalPoint = _value; 
  }

  get GetCustomerTotalPoint() {
    return this.CustomerTotalPoint
  }

  
  constructor() { }
}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamTransaction{

  public service = this.injector.get(ServiceProxy)

  public Receipt : Image = undefined

  public Site = {
    ID : (this.service.UserSiteData.ID != null)? [this.service.UserSiteData.ID] : [],
    Name : (this.service.UserSiteData.Name != null)? [this.service.UserSiteData.Name] : null,
  }

  public Bank = {
    ID : null,
    Name : "",
  }

  public EWallet = {
    ID : null,
    Name : "",
  }

  public Merchant = {
    ID : null,
    Name : "",
    StoreCategoryID : null,
    StoreCategoryName : ""
  }

  public TransactionID = null
  public CustomerID = null

  public ReceiptNumber = null
  public Amount = null

  public CardType = {
    CustomerDataMemberCardType_ID : null,
    ID : null,
    Name : "",
  }

  SetSite(_value) {
    this.Site = _value;
  }
  
  get GetSite() {
    return this.Site
  }
  
  SetBank(_value) {
    this.Bank = _value;
  }

  get GetBank() {
    return this.Bank
  }

  SetEWallet(_value) {
    this.EWallet = _value;
  }

  get GetEWallet() {
    return this.EWallet
  }

  SetMerchant(_value) {
    this.Merchant = _value;
  }

  get GetMerchant() {
    return this.Merchant
  }


  SetTransactionID(_value) {
    this.TransactionID = _value;
  }

  get GetTransactionID() {
    return this.TransactionID
  }

  SetCustomerID(_value) {
    this.CustomerID = _value;
  }

  get GetCustomerID() {
    return this.CustomerID
  }

  SetAmount(_value) {
    this.Amount = _value;
  }

  get GetAmount() {
    return this.Amount
  }

  SetReceiptNumber(_value) {
    this.ReceiptNumber = _value;
  }

  get GetReceiptNumber() {
    return this.ReceiptNumber
  }

  SetCardType(_value) {
    this.CardType = _value;
  }

  get GetCardType() {
    return this.CardType
  }


  SetReceipt(_value) {
    this.Receipt = _value;
  }

  get GetReceipt() {
    return this.Receipt
  }

  
  
  constructor(private injector: Injector,) { }
}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamCustomerRedeem{

  
  public Search = ""
  public StoreCategory = {
    ID : null,
    Name : null,
  }

  public isTransactionSuccess = false
  public transactiomMsg = ""
  public CustomerTransactionHistory_ID = null

  public IsActive = true

  public RedeemID = null

  SetSearch(_value){
    this.Search = _value; 
  }

  get GetSearch() {
    return this.Search
  }

  SetStoreCategory(_value) {
    this.StoreCategory = _value;
  }

  get GetStoreCategory() {
    return this.StoreCategory
  }

  SetIsActive(_value) {
    this.IsActive = _value;
  }

  get GetIsActive() {
    return this.IsActive
  }

  
  SetRedeemID(_value) {
    this.RedeemID = _value;
  }

  get GetRedeemID() {
    return this.RedeemID
  }

  SetIsTransactionSuccess(_value) {
    this.isTransactionSuccess = _value;
  }

  get GetIsTransactionSuccess() {
    return this.isTransactionSuccess
  }

  SetTransactiomMsg(_value) {
    this.transactiomMsg = _value;
  }

  get GetCustomerTransactionHistoryID() {
    return this.CustomerTransactionHistory_ID
  }

  SetCustomerTransactionHistoryID(_value) {
    this.CustomerTransactionHistory_ID = _value;
  }

  get GetTransactiomMsg() {
    return this.transactiomMsg
  }
  
  constructor() { }
}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamPrinterSetting{

  
  public ActionFrom = ""
  public isSelectedPrinter = false

  SetActionFrom(_value){
    this.ActionFrom = _value; 
  }

  get GetActionFrom() {
    return this.ActionFrom
  }

  SetIsSelectedPrinter(_value){
    this.isSelectedPrinter = _value; 
  }

  get GetIsSelectedPrinter() {
    return this.isSelectedPrinter
  }
  
  constructor() { }
}


@Injectable({
  providedIn: 'root'
})
export class GlobalParamChangePassowrd{

  
  public isTransactionSuccess = false
  public transactiomMsg = ""

  SetIsTransactionSuccess(_value) {
    this.isTransactionSuccess = _value;
  }

  get GetIsTransactionSuccess() {
    return this.isTransactionSuccess
  }

  SetTransactiomMsg(_value) {
    this.transactiomMsg = _value;
  }

  get GetTransactiomMsg() {
    return this.transactiomMsg
  }
  
  constructor() { }
}

@Injectable({
  providedIn: 'root'
})
export class GlobalParamCustomerEarning{

  

  public isTransactionSuccess = false
  public transactiomMsg = ""
  public CustomerTransactionHistory_ID = null

  SetIsTransactionSuccess(_value) {
    this.isTransactionSuccess = _value;
  }

  get GetIsTransactionSuccess() {
    return this.isTransactionSuccess
  }

  SetTransactiomMsg(_value) {
    this.transactiomMsg = _value;
  }

  get GetCustomerTransactionHistoryID() {
    return this.CustomerTransactionHistory_ID
  }

  SetCustomerTransactionHistoryID(_value) {
    this.CustomerTransactionHistory_ID = _value;
  }

  get GetTransactiomMsg() {
    return this.transactiomMsg
  }
  
  constructor() { }
}
//-------------------------------------------------------------------------------------------------


@Injectable({
  providedIn: 'root'
})
export class ServiceProxy{
  
  public UserTokenBearer = (ApplicationSettings.getString('TokenBearer') != null)? ApplicationSettings.getString('TokenBearer') : null;
  public UserInformationLogin = (ApplicationSettings.getString('StorageUserLog') != null)? ApplicationSettings.getString('StorageUserLog') : null;
  public UserAccessTime = (ApplicationSettings.getString('UserAccessTime') != null)? ApplicationSettings.getString('UserAccessTime') : null;;
  public UserPassData = (ApplicationSettings.getString('UserPassData') != null)? ApplicationSettings.getString('UserPassData') : null;;

  public ProjectID : number = ProjectID;
  public UserSiteList  = (this.getUserInformation != null)? this.getUserInformation.AppSite : null;
  public UserSiteData  = {
    ID : (this.getUserInformation != null)? this.getUserInformation.AppSite[0].MasterSite_ID : null,
    Name : (this.getUserInformation!= null)? this.getUserInformation.AppSite[0].MasterSite_Name : null,
    Picture : (this.getUserInformation != null)? this.getUserInformation.AppSite[0].MasterSite_Picture : null
  };

  public APIURL  = API_URL;

  public URLBefore = ""

  //Token
  setUserTokenBearer(_value){
    this.UserTokenBearer = _value; 
  }

  get getUserTokenBearer() {
    return JSON.parse(this.UserTokenBearer);
    // return this.UserTokenBearer;
  }

  //Information
  setUserInformation(_value) {
    this.UserInformationLogin = _value;
  }

  get getUserInformation() {
    return JSON.parse(this.UserInformationLogin);
  }

  //Access Time
  setUserAccessTime(_value) {
    this.UserAccessTime = _value;
  }

  get getUserAccessTime() {
    return this.UserAccessTime;
  }

  //User Pass Data
  setUserPassData(_value) {
    this.UserPassData = _value;
  }

  get getUserPassData() {
    return JSON.parse(this.UserPassData);
  }

  //Token
  setUrlBefore(_value){
    this.URLBefore = _value; 
  }

  get getUrlBefore() {
    return this.URLBefore;
    // return this.UserTokenBearer;
  }
  constructor() { }
}
