import { Component, OnInit, ViewContainerRef, ChangeDetectorRef, ViewChildren, ElementRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import { TextField } from "tns-core-modules/ui/text-field"
import * as Dialog from 'ui/dialogs';

import { View, Color } from "tns-core-modules/ui/core/view"
import { EventData } from "tns-core-modules/data/observable";

import { GlobalParamFormula } from '../../../../shared/service-proxy.service';
import { FormulaService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';


@Component({
  selector: 'app-admin-formula-detail',
  templateUrl: './admin-formula-detail.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-formula-detail.component.css'
  ]
})
export class AdminFormulaDetailComponent implements OnInit {

  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamFormula : GlobalParamFormula,
    private _FormulaService :FormulaService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
        
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

      }, 500);
  }

  //Request Formula Detail
  public DetailRequest = {
    Page: null,
    PerPage: null,
    Search : null,
    IsDefault: null,
    IsSpecial: null,
    IsActive: true,
    MasterFormulaPoint_ID: null,
    MasterPointType_ID: null,
    MasterProject_ID: this._SP.ProjectID
  }

  //Formula Detail Data & Paging 
  public DetailData = []
  public AppliesToMsg = ""
  

  //For abort subscribtion search Formula Detail
  public Subscribtion = null;

  //information display for Formula Detail
  public isFormulaDetailLoading = false
  public isFormulaDetailEmpty = true
  public isPullRefresh = null;

  ngOnInit() {
    this.DetailRequest.MasterFormulaPoint_ID = this._GlobalParamFormula.GetFormulaID
    this.GettingDetailFormulaDataAPI(this.DetailRequest,false)

    this._SP.setUrlBefore('/admin/formula')
  }

  //Take By User------------------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/formula'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  refreshDetail(args) {
    this.isPullRefresh = args.object;

    this.GettingDetailFormulaDataAPI(this.DetailRequest, true)
  }

  //Api Function ------------------------------------------------------------------------------------------------
  GettingDetailFormulaDataAPI(_Request,_isPull){
    this.DetailData = []
    this.isFormulaDetailLoading = true
    this.isFormulaDetailEmpty = false
    this.Subscribtion = this._FormulaService.Detail(_Request)
      .subscribe((result: any) => {
        if(result != null){

          if(result.Data != null){
            
            this.DetailData = result.Data

            if(result.Data.MasterFormulaPoint_AppliesToCode == 1){
              this.AppliesToMsg = "Malls"
            }

            if(result.Data.MasterFormulaPoint_AppliesToCode == 2){
              this.AppliesToMsg = "Merchants"
            }

          }else{
            this.DetailData = []
            this.isFormulaDetailEmpty = true
          }
        }else{
          this.DetailData = []
          this.isFormulaDetailEmpty = true
        }
        
        this.isFormulaDetailLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
          title : "OH!",
          message : JSON.stringify(err.error),
          okButtonText : 'Okay'
        }
        Dialog.alert(options).then(()=>{
        })
        
        this.isFormulaDetailLoading = false
        this.isFormulaDetailEmpty = true

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      });
  }
  
}
