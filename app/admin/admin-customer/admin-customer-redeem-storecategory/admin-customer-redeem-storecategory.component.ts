import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import * as Dialog from 'ui/dialogs';

import { Color } from "tns-core-modules/ui/core/view"

import { GlobalParamCustomerRedeem } from '../../../../shared/service-proxy.service';
import { StoreCategoryService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-customer-redeem-storecategory',
  templateUrl: './admin-customer-redeem-storecategory.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-customer-redeem-storecategory.component.css'
  ]
})
export class AdminCustomerRedeemStorecategoryComponent implements OnInit {

   //Request StoreCategory List
   public ListRequest = {
    MasterStoreCategory_ID : null
  }

  //StoreCategory List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}

  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchStoreCategory List
  public Subscribtion = null;

  //information display forStoreCategory List
  public isStoreCategoryListLoading = false
  public isStoreCategoryListEmpty = true
  public isPullRefresh = null;

  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamCustomerRedeem : GlobalParamCustomerRedeem,
    private _StoreCategoryService :StoreCategoryService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

    }, 500);
  }

  ngOnInit() {
    this.GettingListStoreCategoryDataAPI(this.ListRequest,false)

    this._SP.setUrlBefore('/admin/customer-redeem-filter')
  }
  //Take By User------------------------------------------------------------------------------------------------
  refreshList(args) {
    console.log("Refresh 1")
    this.isPullRefresh = args.object;

    
    this.GettingListStoreCategoryDataAPI(this.ListRequest,true)
    // setTimeout(function () {
    //   this.isPullRefresh .refreshing = false;
    //   console.log("Refresh 2")
    // }, 1000);
  }

  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/customer-redeem-filter'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  onItemTap(args: ItemEventData){
    //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);
    this._GlobalParamCustomerRedeem.SetStoreCategory({
      ID : this.ListData[args.index].MasterStoreCategory_ID,
      Name : this.ListData[args.index].MasterStoreCategory_Name
    })
    this.routerExtensions.navigate(['/admin/customer-redeem-filter'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }


  BackAfterListEmpty(){
    this.GettingListStoreCategoryDataAPI(this.ListRequest,false)
  }



  //Funtion Engine to API---------------------------------------------------------------------------------------
  GettingListStoreCategoryDataAPI(_Request,_isPull){
    this.ListData = []
    this.isStoreCategoryListLoading = true
    this.isStoreCategoryListEmpty = false
    this.Subscribtion = this._StoreCategoryService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          //console.log(JSON.stringify(result))

          if(result.Data != null){
            
            this.ListData = result.Data

          }else{
            this.ListData = []
            this.isStoreCategoryListEmpty = true
          }
        }else{
          this.ListData = []
          this.isStoreCategoryListEmpty = true
        }
        
        this.isStoreCategoryListLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
          title : "OH!",
          message : JSON.stringify(err.error),
          okButtonText : 'Okay'
        }
        Dialog.alert(options).then(()=>{
        })
        
        this.isStoreCategoryListLoading = false
        this.isStoreCategoryListEmpty = true

        this._cRef.detectChanges();
      });
  }

}
