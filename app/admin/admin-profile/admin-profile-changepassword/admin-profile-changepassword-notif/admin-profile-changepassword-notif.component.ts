import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { setCurrentOrientation , orientationCleanup} from 'nativescript-screen-orientation';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "tns-core-modules/ui/page"
import { View, Color } from "tns-core-modules/ui/core/view"
import { ScrollView, ScrollEventData } from "tns-core-modules/ui/scroll-view";
import { Animation, AnimationDefinition, CubicBezierAnimationCurve } from "tns-core-modules/ui/animation";
import { SwipeGestureEventData, SwipeDirection, PanGestureEventData, GestureStateTypes } from "tns-core-modules/ui/gestures/gestures";

import * as Dialog from 'ui/dialogs';

import { EventData } from "tns-core-modules/data/observable";
import { isAndroid } from "tns-core-modules/platform";

import { registerElement } from "nativescript-angular/element-registry";
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

import { GlobalParamChangePassowrd } from '../../../../../shared/service-proxy.service';
import { GlobalParamPrinterSetting } from '../../../../../shared/service-proxy.service';
import { CustomerTransactionHistoryService } from '../../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../../shared/service-proxy.service';

import { Hprt, HPRTPrinter } from "nativescript-hprt";
import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";

import * as $ from 'jquery';
const AratakaConfig = require("~/assets/AratakaConfig.js").AratakaConfig;

@Component({
  selector: 'app-admin-profile-changepassword-notif',
  templateUrl: './admin-profile-changepassword-notif.component.html',
  styleUrls: [
     '../../../../assets/local-style.css',
     './admin-profile-changepassword-notif.component.css']
})
export class AdminProfileChangepasswordNotifComponent implements OnInit {

  public isTransaction : boolean = false
  public msg : string = ""

  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _GlobalParamChangePassowrd : GlobalParamChangePassowrd,
    private _GlobalParamPrinterSetting :GlobalParamPrinterSetting,
    private _CustomerTransactionHistoryService : CustomerTransactionHistoryService,
    private _SP : ServiceProxy
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);

  }

  //Take Action ----------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/profile-changepassword'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }


  ngOnInit() {
   this.isTransaction = this._GlobalParamChangePassowrd.GetIsTransactionSuccess
   this.msg = this._GlobalParamChangePassowrd.GetTransactiomMsg

   this._SP.setUrlBefore('/admin/profile-changepassword')
  }

}
