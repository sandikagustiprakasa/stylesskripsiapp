import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { setCurrentOrientation , orientationCleanup} from 'nativescript-screen-orientation';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "tns-core-modules/ui/page"
import { View, Color } from "tns-core-modules/ui/core/view"
import { ScrollView, ScrollEventData } from "tns-core-modules/ui/scroll-view";
import { Animation, AnimationDefinition, CubicBezierAnimationCurve } from "tns-core-modules/ui/animation";
import { SwipeGestureEventData, SwipeDirection, PanGestureEventData, GestureStateTypes } from "tns-core-modules/ui/gestures/gestures";
import * as ApplicationSettings from "application-settings";
import * as Dialog from 'ui/dialogs';

import { EventData } from "tns-core-modules/data/observable";
import { isAndroid } from "tns-core-modules/platform";

import { registerElement } from "nativescript-angular/element-registry";
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

import { ServiceProxy } from '../../../shared/service-proxy.service';


@Component({
	selector: "AdminProfile",
	moduleId: module.id,
	templateUrl: "./admin-profile.component.html",
	styleUrls: [
		'../../assets/local-style.css',
		'./admin-profile.component.css'
	]
})
export class AdminProfileComponent implements OnInit {
	
	public APIURL = this._SP.APIURL;
	public DetailData = undefined

	constructor(
		public page: Page,
		private _cRef: ChangeDetectorRef,
		private routerExtensions: RouterExtensions,
		private modalService: ModalDialogService,
		private viewContainerRef: ViewContainerRef,
		private _SP : ServiceProxy,
	  ) {
		page.actionBarHidden = true;
		setTimeout(function(){ 
		  
		  page.statusBarStyle = "light";
		  page.androidStatusBarBackground = new Color("#F5F5F5");
	
		 }, 500);
	  }

	ngOnInit(): void {
		this.DetailData = this._SP.getUserInformation.Data

		this._SP.setUrlBefore('/admin')
	}


	//Take By User------------------------------------------------------------------------------------------------
	
	ChangePasswordClick(){
		this.routerExtensions.navigate(['/admin/profile-changepassword'],
		{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}

	SignOut(){
		//If user Logged in
		//authenticated
		//TokenBearer
		//StorageUserLog
		//UserAccessTime
		//UserPassData
		//StorageUserSiteList
		//StorageUserSiteData

		const options : Dialog.ConfirmOptions = {
			title : "Confirmation",
			message : "Sign Out?",
			okButtonText : 'Okay',
			cancelButtonText: 'Cancel',
		}
		Dialog.confirm(options).then((result)=>{
			
			if(result == true){
				ApplicationSettings.remove("authenticated");
				ApplicationSettings.remove("TokenBearer");	
				ApplicationSettings.remove("StorageUserLog");
				ApplicationSettings.remove("UserAccessTime");
				ApplicationSettings.remove("UserPassData");
				ApplicationSettings.remove("StorageUserSiteList");
				ApplicationSettings.remove("StorageUserSiteData");

				this.routerExtensions.navigate(['/auth'],
					{
						clearHistory: true,
						animated: true,
						transition: {
							name: "slideRight",
							duration: 300,
							curve: "easeIn"
						}
					}
				);
			}
		})


	}

	BackToPageBefore(){
		this.routerExtensions.navigate(['/admin'],
				{
					clearHistory: true,
					animated: true,
					transition: {
						name: "slideRight",
						duration: 300,
						curve: "easeIn"
					}
				}
			);
	  }

}