import { Component, OnInit} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"

import { Color } from "tns-core-modules/ui/core/view"
import { ServiceProxy } from '../../../../shared/service-proxy.service';
import { GlobalParamMerchant } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-merchant-filter',
  templateUrl: './admin-merchant-filter.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-merchant-filter.component.css'
  ]
})
export class AdminMerchantFilterComponent implements OnInit {

  public StoreCategoryFilter = {
    ID : null,
    Name : ""
  };

  public BrandFilter = {
    ID : null,
    Name : ""
  };

  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _GlobalParamMerchant : GlobalParamMerchant,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this.StoreCategoryFilter = this._GlobalParamMerchant.GetStoreCategory
    this.BrandFilter = this._GlobalParamMerchant.GetBrand

    this._SP.setUrlBefore('/admin/merchant')
  }

  //Take By User------------------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/merchant'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoFilter(){
    this.routerExtensions.navigate(['/admin/merchant'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToStoreCategoryPage(){

		this.routerExtensions.navigate(['/admin/merchant-storecategory'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToBrandPage(){

		this.routerExtensions.navigate(['/admin/merchant-brand'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  TimesClick(_Filter){
    if(_Filter == 'storecategory'){
      this.StoreCategoryFilter.ID = null
      this.StoreCategoryFilter.Name = ""
      this._GlobalParamMerchant.SetStoreCategory(this.StoreCategoryFilter)
    }

    if(_Filter == 'brand'){
      this.BrandFilter.ID = null
      this.BrandFilter.Name = ""
      this._GlobalParamMerchant.SetStoreCategory(this.BrandFilter)
    }
  }

}
