import { Component, OnInit , ViewChildren} from "@angular/core";
import { Page } from "ui/page";
import { RouterExtensions } from "nativescript-angular/router";

import * as ApplicationSettings from "application-settings";

import * as Dialog from 'ui/dialogs';
import * as NativeApps from 'tns-core-modules/ui/frame'; 
import * as ApplicationApps from "tns-core-modules/application";

import { Color } from "tns-core-modules/ui/core/view"

import { AuthService } from '../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../shared/service-proxy.service';
import { HttpErrorResponse } from '@angular/common/http';


import * as $ from 'jquery';
//declare const AratakaConfig : any;
const AratakaConfig = require("~/assets/AratakaConfig.js").AratakaConfig;



@Component({
	selector: "AuthSignin",
	moduleId: module.id,
	templateUrl: "./auth-signin.component.html",
	styleUrls: [
		'../../assets/local-style.css',
		'./auth-signin.component.css'
	]
})
export class AuthSigninComponent implements OnInit {

	@ViewChildren('inputRequired') inputRequired;

	public AuthLoading = false
	
	//Site Select
	public SiteDataGlobalList = [];
	public FilterSiteFrmChild = {
		ID : null,
		Name : null
	};

	//Model
	public Username: string = "abrisam.arzayn";
	public Password: string = "12345";

	//SignInRequest
	private SignInRequest = {
		Username : "",
		Password : ""
	}


	constructor(
		private page: Page,
		private routerExtensions: RouterExtensions,
		private _AuthService : AuthService,
		private _SP : ServiceProxy,
	) {
		page.actionBarHidden = true;
		setTimeout(function(){ 
	
			page.statusBarStyle = "dark";
			page.androidStatusBarBackground = new Color("#004D40");
	
		}, 500);
	}

	// goBack(): void {
	// 	this.routerExtensions.back();
	// }

	ngOnInit(): void {
		var t = this
		setTimeout(function(){
			//t.SystemUIAndroid()
		},1000)

		
		
		if(ApplicationSettings.getBoolean("authenticated") != undefined || ApplicationSettings.getBoolean("authenticated") == true) {
            this.routerExtensions.navigate(['/admin'],
				{
					clearHistory: true,
				}
			);
        }
	}


	ForgotPasswordClick(){
		this.routerExtensions.navigate(['/auth/forgotpassword/'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}


	LoginButton() {

		const options : Dialog.AlertOptions = {
			title : "OH!",
			message : "Username or Password is Wrong!",
			okButtonText : 'Okay'
		}
		

		var valid = true;
		this.inputRequired.forEach(element => {
			//console.log(element.nativeElement.text)
			if(element.nativeElement.text == ""){
				valid = false;
				return false;
			}
		});


		if(valid == true){
			
			this.SignInRequest.Username = this.Username
			// this.SignInRequest.Password = this.Password
			this.SignInRequest.Password = AratakaConfig.Sha256(this.Password)
			console.log(AratakaConfig.Sha256(this.Password))
			var grantType = "username=" +  this.SignInRequest.Username + "&password=" +this.SignInRequest.Password + "&grant_type=password";
      		this.GetToken(grantType,this.SignInRequest);

			//this.DoProccess(this.SignInRequest)

		}
		
	}

	GetToken(_GrantType,_Request){

		this.AuthLoading = true
	
		this._AuthService.Token(_GrantType)
		  .subscribe((result: any) => {
			//console.log(JSON.stringify(result))

			if(result.access_token != undefined){
			  
			  //set user token
			  ApplicationSettings.setString('TokenBearer', JSON.stringify(result));
			  this._SP.setUserTokenBearer(ApplicationSettings.getString('TokenBearer'))
				
			  //this._SP.setUserTokenBearer(JSON.stringify(result))

			  this.DoProccess(_Request);
	
			}else{
				const options : Dialog.AlertOptions = {
					title : "OH!",
					message : "Username or Passwor is wrong!",
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
				})

				this.AuthLoading = false
			}
	
		  },
		  (err: HttpErrorResponse) => {
			console.log(JSON.stringify(err));
			alert(JSON.stringify(err.error.error_description))
			this.AuthLoading = false
		  });
	}
	

	DoProccess(_Request){
		this.AuthLoading = true
		this.Password = ""
		this._AuthService.GetUserData(_Request)
		  .subscribe((result: any) => {
			console.log(JSON.stringify(result));
			if (result.Code == 1) {
				
				if(result.Data.MasterJobPosition_ID != 3)
				{
					ApplicationSettings.remove("authenticated");
					ApplicationSettings.remove("TokenBearer");	
					ApplicationSettings.remove("StorageUserLog");
					ApplicationSettings.remove("UserAccessTime");
					ApplicationSettings.remove("UserPassData");
					ApplicationSettings.remove("StorageUserSiteList");
					ApplicationSettings.remove("StorageUserSiteData");
					const options : Dialog.AlertOptions = {
						title : "OH!",
						message : "Only Customer Service Account!",
						okButtonText : 'Okay'
					}
					Dialog.alert(options).then(()=>{
					})
				}
				else
				{
					ApplicationSettings.setBoolean("authenticated", true);
					
					ApplicationSettings.setString('StorageUserLog', JSON.stringify(result));
					this._SP.setUserInformation(ApplicationSettings.getString('StorageUserLog'))
			
					var Today = new Date()
					ApplicationSettings.setString('UserAccessTime',Today.toString())
					this._SP.setUserAccessTime(ApplicationSettings.getString('UserAccessTime'))
		
					ApplicationSettings.setString('UserPassData',JSON.stringify(_Request))
					this._SP.setUserPassData(ApplicationSettings.getString('UserPassData'))

					//Site
					this._SP.UserSiteList = result.AppSite;
					this._SP.UserSiteData.ID = result.AppSite[0].MasterSite_ID;
					this._SP.UserSiteData.Name = result.AppSite[0].MasterSite_Name;
					this._SP.UserSiteData.Picture = result.AppSite[0].MasterSite_Picture;
					
					ApplicationSettings.setString('StorageUserSiteList', JSON.stringify(this.SiteDataGlobalList));
					ApplicationSettings.setString('StorageUserSiteData', JSON.stringify(this.FilterSiteFrmChild));


					this.routerExtensions.navigate(['/admin'],
						{
							clearHistory: true,
							animated: true,
							transition: {
								name: "slideLeft",
								duration: 300,
								curve: "easeIn"
							}
						}
					);
				}
				
			}else{
				const options : Dialog.AlertOptions = {
					title : "OH!",
					message : result.Message,
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
				})
			}

			this.AuthLoading = false
		  },
		  (err: HttpErrorResponse) => {
			console.log(JSON.stringify(err));
			alert(JSON.stringify(err))
			this.AuthLoading = false
		  });
	
	}
	

	SystemUIAndroid(){
		
		NativeApps.Frame.topmost().android.activity.getWindow().getDecorView()
		.setSystemUiVisibility(
			android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
			android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
		)

		ApplicationApps.android.on(ApplicationApps.AndroidApplication.activityCreatedEvent, (event) => {

			const activity = event.activity;


			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
				activity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
				activity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
				activity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
				activity.getWindow().setStatusBarColor(android.graphics.Color.TRANSPARENT);
			} else {
				activity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			}

			const parent = activity.findViewById(android.R.id.content);
			for (let i = 0; i < parent.getChildCount(); i++) {
				const childView = parent.getChildAt(i);
				if (childView instanceof android.view.ViewGroup) {
					childView.setFitsSystemWindows(true);
					childView.setClipToPadding(true);
				}
			}
		});
	
	}

	//authenticated
	//TokenBearer
	//StorageUserLog
	//UserAccessTime
	//UserPassData
	//StorageUserSiteList
	//StorageUserSiteData
}