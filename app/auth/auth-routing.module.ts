import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AuthSigninComponent } from "./auth-signin/auth-signin.component";
import { AuthForgotpasswordComponent } from './auth-forgotpassword/auth-forgotpassword.component';


const routes: Routes = [
    { path: "", component: AuthSigninComponent },
    { path: "forgotpassword", component: AuthForgotpasswordComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AuthRoutingModule { }
