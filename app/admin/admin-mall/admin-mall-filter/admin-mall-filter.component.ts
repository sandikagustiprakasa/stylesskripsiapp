import { Component, OnInit} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"

import { Color } from "tns-core-modules/ui/core/view"

import { GlobalParamSite } from '../../../../shared/service-proxy.service';

import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-mall-filter',
  templateUrl: './admin-mall-filter.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-mall-filter.component.css'
  ]
})
export class AdminMallFilterComponent implements OnInit {

  public ProvinceFilter = {
    ID : null,
    Name : ""
  };

  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _GlobalParamSite : GlobalParamSite,
    private _SP : ServiceProxy
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this.ProvinceFilter = this._GlobalParamSite.GetProvince

    this._SP.setUrlBefore('/admin/mall')
  }

  //Take By User------------------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/mall'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoFilter(){
    this.routerExtensions.navigate(['/admin/mall'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToProvincePage(){

		this.routerExtensions.navigate(['/admin/mall-province'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  TimesClick(_Filter){
    if(_Filter == 'Province'){
      this.ProvinceFilter.ID = null
      this.ProvinceFilter.Name = ""
      this._GlobalParamSite.SetProvince(this.ProvinceFilter)
    }
    
  }
}
