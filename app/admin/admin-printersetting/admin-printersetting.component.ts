import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import * as Dialog from 'ui/dialogs';

import { Color } from "tns-core-modules/ui/core/view"

import { GlobalParamPrinterSetting } from '../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../shared/service-proxy.service';

import { Hprt, HPRTPrinter } from "nativescript-hprt";

import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";


@Component({
  selector: 'app-admin-printersetting',
  templateUrl: './admin-printersetting.component.html',
  styleUrls: [
    '../../assets/local-style.css',
    './admin-printersetting.component.css']
})
export class AdminPrintersettingComponent implements OnInit {

  public CustomText : string = ""

  private hprt: Hprt;

  text: string;
  SelectedPrinter: boolean;
  BtnBluetoothEnabled: boolean;

  loader: any; 

  //PrinterSetting List Data & Paging 
  public ListData : Array<HPRTPrinter>;
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchPrinterSetting List
  public Subscribtion = null;

  public isPrinterSettingListEmpty = true

  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _SP : ServiceProxy,
    private _GlobalParamPrinterSetting : GlobalParamPrinterSetting
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");
     }, 500);

    this.hprt = new Hprt();
    this.SelectedPrinter = false;
    this.BtnBluetoothEnabled = this.hprt.isBluetoothEnabled();
    this.loader = new LoadingIndicator();

  }

  ngOnInit() {
    this.SelectedPrinter = this._GlobalParamPrinterSetting.GetIsSelectedPrinter
    if(this.SelectedPrinter){
      this.searchPrinters()
    }

    this._SP.setUrlBefore('/admin')
  }
  //Take By User------------------------------------------------------------------------------------------------

  enableBluetooth() {

      console.log("Enabling bluetooth...");
      this.loader.show();

      this.hprt.enableBluetooth().then((res) => {
          console.log("Enabled", res);
          this.BtnBluetoothEnabled = true;
          this.loader.hide();
      }, (err) => {
          console.log("Error", err);
          this.loader.hide();
      });
  }

  isBluetoothEnabled() {
      return this.hprt.isBluetoothEnabled();
  }

  searchPrinters() {
    
    if(this.isBluetoothEnabled()){
      this.hprt.searchPrinters().then(printers => {
          this.ListData = printers;
      });
    }else{

      const options : Dialog.AlertOptions = {
        title : "OH!",
        message : "Please enable Bluetooth first!",
        okButtonText : 'Understand'
      }
      Dialog.alert(options).then(()=>{
      })
    }
  }

  disconnect() {

      this.hprt.disconnect().then((res) => {
          console.log("success", res);
          this.SelectedPrinter = false;
          this._GlobalParamPrinterSetting.SetIsSelectedPrinter(this.SelectedPrinter)
      }, (err) => {
          console.log("error", err)
      })

      

  } 

  BackToPageBefore(){
    var addres = ""
    if(this._GlobalParamPrinterSetting.GetActionFrom == "app-admin-customer-redeem-notiftrans"){
      addres = "/admin/customer-redeem/notiftrans"
    }
    else if(this._GlobalParamPrinterSetting.GetActionFrom == "app-admin-customer-trans-history"){
      addres = "/admin/customer-trans-history"
    }
    else if(this._GlobalParamPrinterSetting.GetActionFrom == "app-admin-customer-transaction-notif"){
      addres = "/admin/customer-transaction/notif"
    }else{
      addres = "/admin"
    }

    console.log(addres)
    this.routerExtensions.navigate([addres],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  onItemTap(args: ItemEventData){
    //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);

    this.loader.show();

    this.hprt.connect(this.ListData[args.index]).then((res) => {
        console.log("success", res);
        this.SelectedPrinter = true;
        this._GlobalParamPrinterSetting.SetIsSelectedPrinter(this.SelectedPrinter)
        this.loader.hide();
    }, (err) => {
        console.log("error", err)
        this.loader.hide();
    })
  }

}
