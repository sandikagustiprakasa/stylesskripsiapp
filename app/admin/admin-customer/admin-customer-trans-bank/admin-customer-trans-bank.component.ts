import { Component, OnInit, ViewContainerRef, ChangeDetectorRef, ViewChildren, ElementRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import { TextField } from "tns-core-modules/ui/text-field"
import * as Dialog from 'ui/dialogs';

import { View, Color } from "tns-core-modules/ui/core/view"
import { EventData } from "tns-core-modules/data/observable";

import { GlobalParamTransaction } from '../../../../shared/service-proxy.service';
import { BankService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-customer-trans-bank',
  templateUrl: './admin-customer-trans-bank.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-customer-trans-bank.component.css'
  ]
})
export class AdminCustomerTransBankComponent implements OnInit {

  //Filter model
  public keyword : string = ""

  
  //RequestBank List
  public ListRequest = {
    Page: 1,
    PerPage: 20,
    Search : "",
    MasterBank_ID: null
  }

  //Bank List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchBank List
  public Subscribtion = null;

  //information display forBank List
  public isBankListLoading = false
  public isBankListEmpty = true
  public isPullRefresh = null;
  public isLoadMoreItem = false;
  public LoadMoreItemMsg = "";

  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamTransaction : GlobalParamTransaction,
    private _BankService : BankService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
        
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

    }, 500);
  }

  ngOnInit() {
    
    this.GettingListBankDataAPI(this.ListRequest,false)
    this.isLoadMoreItem = false
    this.LoadMoreItemMsg = ""

    this._SP.setUrlBefore('/admin/customer-transaction')
  }

  //Mobile Code ------------------------------------------------------------------------------------------------
  public wrapperLayout : View = undefined;
  public searchBar : TextField = undefined;


  onWrapperLoaded(args : EventData){
        
    this.wrapperLayout = args.object as View;
    const page = this.wrapperLayout.page;

    this.searchBar = page.getViewById("searchBar") as TextField;

    //this.searchBar.focus();
  }

  //Take By User------------------------------------------------------------------------------------------------
  refreshList(args) {
    console.log("Refresh 1")
    this.isPullRefresh = args.object;

    this.ListRequest.Page = 1
    this.ListRequest.PerPage = 20   
    
    this.GettingListBankDataAPI(this.ListRequest,true)
    // setTimeout(function () {
    //   this.isPullRefresh .refreshing = false;
    //   console.log("Refresh 2")
    // }, 1000);
  }

  loadMoreItems() {
    if(this.ListRequest.Page < Math.ceil(this.ListDataPaging.count / this.ListRequest.PerPage)){
      this.ListRequest.Page = this.ListRequest.Page + 1 
      this.GettingListBankDataLoadMoreItemAPI(this.ListRequest)
    }
  }

  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/customer-transaction'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  onItemTap(args: ItemEventData){
    //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);
    this._GlobalParamTransaction.SetBank({
      ID : this.ListData[args.index].MasterBank_ID,
      Name : this.ListData[args.index].MasterBank_Name
    })

    this.routerExtensions.navigate(['/admin/customer-transaction'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }


  KeywordReturnPress(){
    this.ListRequest.Search = this.keyword      
    this.DoSearch()
  }

  KeywordChange(){
    
    this.ListRequest.Search = this.keyword      
    this.DoSearch()

  }

  DoSearch(){
      
    this.ListRequest.Page = 1
      if(this.Subscribtion){
      this.Subscribtion.unsubscribe()
    }
    this.GettingListBankDataAPI(this.ListRequest,false)    

  }

  BackAfterListEmpty(){
    this.keyword = ""
    this.ListRequest.Search = this.keyword    
    this.GettingListBankDataAPI(this.ListRequest,false)
  }


  //Funtion Engine to API---------------------------------------------------------------------------------------
  GettingListBankDataAPI(_Request,_isPull){
    this.ListData = []
    this.isBankListLoading = true
    this.isBankListEmpty = false
    this.Subscribtion = this._BankService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          //console.log(JSON.stringify(result))

          if(result.Data != null){
            
            this.ListData = result.Data
            this.ListDataPaging.count = result.TotalData

          }else{
            this.ListData = []
            this.isBankListEmpty = true
          }
        }else{
          this.ListData = []
          this.isBankListEmpty = true
        }
        
        this.isBankListLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
          title : "OH!",
          message : JSON.stringify(err.error),
          okButtonText : 'Okay'
        }
        Dialog.alert(options).then(()=>{
        })
        
        this.isBankListLoading = false
        this.isBankListEmpty = true

        this._cRef.detectChanges();
      });
  }

  GettingListBankDataLoadMoreItemAPI(_Request){
    this.isLoadMoreItem = true
    this.LoadMoreItemMsg = "Load Data.."
    this.Subscribtion = this._BankService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          
          if(result.Data != null){
            console.log("More Item", JSON.stringify(result))
            this.ListData = this.ListData.concat(result.Data)

          }else{
            this.LoadMoreItemMsg = "You have reach all data"
          }
        }else{
          this.LoadMoreItemMsg = "There is some error, pleas refresh and try again"
        }
        
        this.isLoadMoreItem = false
        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
					title : "OH!",
					message : "Load More item : " + JSON.stringify(err.error),
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
        })

        this.LoadMoreItemMsg = "Load More item : " + JSON.stringify(err.error)
        this.isLoadMoreItem = false

        this._cRef.detectChanges();
      });
  }

}
