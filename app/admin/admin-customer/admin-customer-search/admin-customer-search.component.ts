import { Component, OnInit, ViewContainerRef, ChangeDetectorRef, ViewChildren, ElementRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import { TextField } from "tns-core-modules/ui/text-field"
import * as Dialog from 'ui/dialogs';

import { View, Color } from "tns-core-modules/ui/core/view"
import { EventData } from "tns-core-modules/data/observable";

import { GlobalParamCustomer } from '../../../../shared/service-proxy.service';
import { CustomerService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';


@Component({
  selector: 'app-admin-customer-search',
  templateUrl: './admin-customer-search.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-customer-search.component.css'
  ]
})
export class AdminCustomerSearchComponent implements OnInit {

  //Filter model
  public keyword : string = ""

  //RequestCustomer List
  public ListRequest = {
    Page: 1,
    PerPage: 10,
    Search : "",
    SearchCardNumber: null,
    MasterCustomerData_ID : null,
    Status : true,
    MasterProject_ID: this._SP.ProjectID
  }

  //Customer List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchCustomer List
  public Subscribtion = null;

  //information display forCustomer List
  public isCustomerListLoading = false
  public isCustomerListEmpty = true

constructor(
  public page: Page,
  private _cRef: ChangeDetectorRef,
  private routerExtensions: RouterExtensions,
  private modalService: ModalDialogService,
  private viewContainerRef: ViewContainerRef,
  private _GlobalParamCustomer : GlobalParamCustomer,
  private _CustomerService :CustomerService,
  private _SP : ServiceProxy,
) {
  page.actionBarHidden = true;
  setTimeout(function(){ 
      
    page.statusBarStyle = "light";
    page.androidStatusBarBackground = new Color("#F5F5F5");

   }, 500);
}

ngOnInit() {
   
  this.keyword = this._GlobalParamCustomer.GetSearch

  this.GettingListCustomerDataAPI(this.ListRequest)
 
  this._SP.setUrlBefore('/admin/customer')
}

//Mobile Code ------------------------------------------------------------------------------------------------
public wrapperLayout : View = undefined;
public searchBar : TextField = undefined;


onWrapperLoaded(args : EventData){
      
  this.wrapperLayout = args.object as View;
  const page = this.wrapperLayout.page;

  this.searchBar = page.getViewById("searchBar") as TextField;

  this.searchBar.focus();
}

//Take By User------------------------------------------------------------------------------------------------

BackToPageBefore(){
  this.routerExtensions.navigate(['/admin/customer'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}

onItemTap(args: ItemEventData){
  //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);

  this._GlobalParamCustomer.SetSearch(this.ListData[args.index].MasterCustomerData_Name)

  this.routerExtensions.navigate(['/admin/customer'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}


KeywordReturnPress(){
  this.ListRequest.Search = this.keyword      
  this.DoSearch()
}

KeywordChange(){
  
  this.ListRequest.Search = this.keyword      
  this.DoSearch()

}

DoSearch(){
    
  this.ListRequest.Page = 1
    if(this.Subscribtion){
    this.Subscribtion.unsubscribe()
  }
  this.GettingListCustomerDataAPI(this.ListRequest)    

}

BackAfterListEmpty(){
  this.keyword = ""
  this.ListRequest.Search = this.keyword    
  this.GettingListCustomerDataAPI(this.ListRequest)
}

TimesClick(){
  this._GlobalParamCustomer.SetSearch("")
  this.routerExtensions.navigate(['/admin/customer'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}


//Funtion Engine to API---------------------------------------------------------------------------------------
GettingListCustomerDataAPI(_Request){
  this.ListData = []
  this.isCustomerListLoading = true
  this.isCustomerListEmpty = false
  this.Subscribtion = this._CustomerService.List(_Request)
    .subscribe((result: any) => {
      if(result != null){
        //console.log(JSON.stringify(result))

        if(result.Data != null){
          
          this.ListData = result.Data
          this.ListDataPaging.count = result.TotalData

        }else{
          this.ListData = []
          this.isCustomerListEmpty = true
        }
      }else{
        this.ListData = []
        this.isCustomerListEmpty = true
      }
      
      this.isCustomerListLoading = false

      this._cRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
      console.log("Error!", JSON.stringify(err.error))

      const options : Dialog.AlertOptions = {
        title : "OH!",
        message : JSON.stringify(err.error),
        okButtonText : 'Okay'
      }
      Dialog.alert(options).then(()=>{
      })
      
      this.isCustomerListLoading = false
      this.isCustomerListEmpty = true

      this._cRef.detectChanges();
    });
  }

}
