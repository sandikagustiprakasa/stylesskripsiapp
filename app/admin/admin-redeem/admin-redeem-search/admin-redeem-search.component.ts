import { Component, OnInit, ViewContainerRef, ChangeDetectorRef, ViewChildren, ElementRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import { TextField } from "tns-core-modules/ui/text-field"
import * as Dialog from 'ui/dialogs';

import { View, Color } from "tns-core-modules/ui/core/view"
import { EventData } from "tns-core-modules/data/observable";

import { GlobalParamRedeem } from '../../../../shared/service-proxy.service';
import { RedeemService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-redeem-search',
  templateUrl: './admin-redeem-search.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-redeem-search.component.css'
  ]
})
export class AdminRedeemSearchComponent implements OnInit {

  //Filter model
  public keyword : string = ""

  //RequestRedeem List
  public ListRequest = {
    Page: 1,
    PerPage: 5,
    Search : "",
    IsActive : null,
    MasterRedeem_ID : null,
    MasterStoreCategory_ID : null,
    MasterProject_ID: this._SP.ProjectID,
  }

  //Redeem List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchRedeem List
  public Subscribtion = null;

  //information display forRedeem List
  public isRedeemListLoading = false
  public isRedeemListEmpty = true

constructor(
  public page: Page,
  private _cRef: ChangeDetectorRef,
  private routerExtensions: RouterExtensions,
  private modalService: ModalDialogService,
  private viewContainerRef: ViewContainerRef,
  private _GlobalParamRedeem : GlobalParamRedeem,
  private _RedeemService :RedeemService,
  private _SP : ServiceProxy,
) {
  page.actionBarHidden = true;
  setTimeout(function(){ 
      
    page.statusBarStyle = "light";
    page.androidStatusBarBackground = new Color("#F5F5F5");

   }, 500);
}

ngOnInit() {
   
  this.keyword = this._GlobalParamRedeem.GetSearch

  this.GettingListRedeemDataAPI(this.ListRequest)

  this._SP.setUrlBefore('/admin/redeem')
  
}

//Mobile Code ------------------------------------------------------------------------------------------------
public wrapperLayout : View = undefined;
public searchBar : TextField = undefined;


onWrapperLoaded(args : EventData){
      
  this.wrapperLayout = args.object as View;
  const page = this.wrapperLayout.page;

  this.searchBar = page.getViewById("searchBar") as TextField;

  this.searchBar.focus();
}

//Take By User------------------------------------------------------------------------------------------------

BackToPageBefore(){
  this.routerExtensions.navigate(['/admin/redeem'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}

onItemTap(args: ItemEventData){
  //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);

  this._GlobalParamRedeem.SetSearch(this.ListData[args.index].MasterRedeem_Name)

  this.routerExtensions.navigate(['/admin/redeem'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}


KeywordReturnPress(){
  this.ListRequest.Search = this.keyword      
  this.DoSearch()
}

KeywordChange(){
  
  this.ListRequest.Search = this.keyword      
  this.DoSearch()

}

DoSearch(){
    
  this.ListRequest.Page = 1
    if(this.Subscribtion){
    this.Subscribtion.unsubscribe()
  }
  this.GettingListRedeemDataAPI(this.ListRequest)    

}

BackAfterListEmpty(){
  this.keyword = ""
  this.ListRequest.Search = this.keyword    
  this.GettingListRedeemDataAPI(this.ListRequest)
}

TimesClick(){
  this._GlobalParamRedeem.SetSearch("")
  this.routerExtensions.navigate(['/admin/redeem'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}


//Funtion Engine to API---------------------------------------------------------------------------------------
GettingListRedeemDataAPI(_Request){
  this.ListData = []
  this.isRedeemListLoading = true
  this.isRedeemListEmpty = false
  this.Subscribtion = this._RedeemService.List(_Request)
    .subscribe((result: any) => {
      if(result != null){
        //console.log(JSON.stringify(result))

        if(result.Data != null){
          
          this.ListData = result.Data
          this.ListDataPaging.count = result.TotalData

        }else{
          this.ListData = []
          this.isRedeemListEmpty = true
        }
      }else{
        this.ListData = []
        this.isRedeemListEmpty = true
      }
      
      this.isRedeemListLoading = false

      this._cRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
      console.log("Error!", JSON.stringify(err.error))

      const options : Dialog.AlertOptions = {
        title : "OH!",
        message : JSON.stringify(err.error),
        okButtonText : 'Okay'
      }
      Dialog.alert(options).then(()=>{
      })
      
      this.isRedeemListLoading = false
      this.isRedeemListEmpty = true

      this._cRef.detectChanges();
    });
  }

}
