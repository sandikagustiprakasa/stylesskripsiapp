import { Component } from '@angular/core';
import * as application from "tns-core-modules/application";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "tns-core-modules/application";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { isAndroid } from "tns-core-modules/platform";
import * as Dialog from 'ui/dialogs';

import * as ApplicationSettings from "application-settings";
import { HttpErrorResponse } from '@angular/common/http';

import { AuthService } from '../shared/service-proxy.service';
import { ServiceProxy } from '../shared/service-proxy.service';

import { LoadingIndicator,  Mode, OptionsCommon } from '@nstudio/nativescript-loading-indicator';

@Component({
  selector: 'app-root',
  template: `<page-router-outlet></page-router-outlet>`
})
export class AppComponent {

  
  public indicator = new LoadingIndicator();

  constructor(
    private routerExtensions: RouterExtensions,
    private router: Router,
    private _AuthService : AuthService,
		private _SP : ServiceProxy,
  ) {
  }

  ngOnInit(): void {

			
    if(ApplicationSettings.getBoolean("authenticated") != undefined || ApplicationSettings.getBoolean("authenticated") == true) {
        this.routerExtensions.navigate(['/admin'],
        {
          clearHistory: true,
        });
        
        const options: OptionsCommon = {
          message: 'Communicate with Rest API',
          details: '',
          progress: 0.65,
          margin: 10,
          dimBackground: true,
          color: '#000', // color of indicator and labels
          // background box around indicator
          // hideBezel will override this if true
          backgroundColor: 'white',
          userInteractionEnabled: false, // default true. Set false so that the touches will fall through it.
          hideBezel: true, // default false, can hide the surrounding bezel
          mode: Mode.AnnularDeterminate, // see options below
        };
        
        this.indicator.show(options);

        var grantType = "username=" +  this._SP.getUserPassData.Username + "&password=" + this._SP.getUserPassData.Password + "&grant_type=password";
        this.GetToken(grantType)

    }else{
      this.routerExtensions.navigate(['/auth'],
      {
        clearHistory: true,
      });
    }


    if(isAndroid){
      application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        
 

        if (this.router.isActive("/admin", true)) {
          data.cancel = false;
        }
        else{
          data.cancel = true;
          this.routerExtensions.navigate([this._SP.getUrlBefore],
            {
              clearHistory: false,
              animated: true,
              transition: {
                name: "slideRight",
                duration: 300,
                curve: "easeIn"
              }
            }
          );
        }
      });
    }


  }
  
  GetToken(_GrantType){

		this._AuthService.Token(_GrantType)
		  .subscribe((result: any) => {
			//console.log(JSON.stringify(result))

			if(result.access_token != undefined){
			  
			  //set user token
			  ApplicationSettings.setString('TokenBearer', JSON.stringify(result));
			  this._SP.setUserTokenBearer(ApplicationSettings.getString('TokenBearer'))
				
	
			}else{
				const options : Dialog.AlertOptions = {
					title : "OH!",
					message : "Failed to connect with Rest API",
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
				})

			}
      
      this.indicator.hide();

		  },
		  (err: HttpErrorResponse) => {
        console.log(JSON.stringify(err));
        const options : Dialog.AlertOptions = {
          title : "OH!",
          message : err.error.error_description,
          okButtonText : 'Okay'
        }
        Dialog.alert(options).then(()=>{
        })

        this.indicator.hide();

		  });
	}
	

  // ngOnInit() {
    
    
  //   // if(isAndroid){
  //   //   application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        
 

  //   //     if (this.router.isActive("/admin", true)) {
  //   //       data.cancel = false;
  //   //     }
  //   //     else if (this.router.isActive("/admin/merchant", true)) {
  //   //       data.cancel = true;
  //   //       this.routerExtensions.navigate(['/admin'],
  //   //         {
  //   //           clearHistory: false,
  //   //           animated: true,
  //   //           transition: {
  //   //             name: "slideRight",
  //   //             duration: 300,
  //   //             curve: "easeIn"
  //   //           }
  //   //         }
  //   //       );
  //   //     }else if (this.router.isActive("/admin/merchant-search", true)) {
  //   //       data.cancel = true;
  //   //       this.routerExtensions.navigate(['/admin/merchant'],
  //   //         {
  //   //           clearHistory: false,
  //   //           animated: true,
  //   //           transition: {
  //   //             name: "slideRight",
  //   //             duration: 300,
  //   //             curve: "easeIn"
  //   //           }
  //   //         }
  //   //       );
  //   //     }else{
          
  //   //     }

  //   //   });
  //   // }

  // }
}
