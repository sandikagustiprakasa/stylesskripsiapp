import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';

import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { AdminRoutingModule } from "./admin-routing.module";

import { AdminFirstPageComponent } from "./admin-first-page/admin-first-page.component";
import { AdminHomeComponent } from "./admin-home/admin-home.component";
import { AdminHomeModal1Component } from "./admin-home/modal/admin-home-modal-1.component";
import { AdminProfileComponent } from "./admin-profile/admin-profile.component";
import { AdminHistoryComponent } from "./admin-history/admin-history.component";
import { AdminModalHistory1Component } from "./admin-history/modal-history/admin-modal-history.component";
import { AdminTestprintComponent } from './admin-testprint/admin-testprint.component';
import { AdminFirstPageSwitchsiteComponent } from './admin-first-page/admin-first-page-switchsite/admin-first-page-switchsite.component';
import { AdminMallComponent } from './admin-mall/admin-mall.component';
import { AdminMallDetailComponent } from './admin-mall/admin-mall-detail/admin-mall-detail.component';
import { AdminMallFilterComponent } from './admin-mall/admin-mall-filter/admin-mall-filter.component';
import { AdminMallProvinceComponent } from './admin-mall/admin-mall-province/admin-mall-province.component';
import { AdminMallSearchComponent } from './admin-mall/admin-mall-search/admin-mall-search.component';
import { AdminBrandComponent } from './admin-brand/admin-brand.component';
import { AdminBrandFilterComponent } from './admin-brand/admin-brand-filter/admin-brand-filter.component';
import { AdminBrandSearchComponent } from './admin-brand/admin-brand-search/admin-brand-search.component';
import { AdminBrandDetailComponent } from './admin-brand/admin-brand-detail/admin-brand-detail.component';
import { AdminBrandStorecategoryComponent } from './admin-brand/admin-brand-storecategory/admin-brand-storecategory.component';
import { AdminMerchantComponent } from './admin-merchant/admin-merchant.component';
import { AdminMerchantDetailComponent } from './admin-merchant/admin-merchant-detail/admin-merchant-detail.component';
import { AdminMerchantFilterComponent } from './admin-merchant/admin-merchant-filter/admin-merchant-filter.component';
import { AdminMerchantSearchComponent } from './admin-merchant/admin-merchant-search/admin-merchant-search.component';
import { AdminMerchantStorecategoryComponent } from './admin-merchant/admin-merchant-storecategory/admin-merchant-storecategory.component';
import { AdminMerchantBrandComponent } from './admin-merchant/admin-merchant-brand/admin-merchant-brand.component';
import { AdminCardtypeComponent } from './admin-cardtype/admin-cardtype.component';
import { AdminCardtypeDetailComponent } from './admin-cardtype/admin-cardtype-detail/admin-cardtype-detail.component';
import { AdminCardtypeSearchComponent } from './admin-cardtype/admin-cardtype-search/admin-cardtype-search.component';
import { AdminFormulaComponent } from './admin-formula/admin-formula.component';
import { AdminFormulaDetailComponent } from './admin-formula/admin-formula-detail/admin-formula-detail.component';
import { AdminFormulaSearchComponent } from './admin-formula/admin-formula-search/admin-formula-search.component';
import { AdminFormulaFilterComponent } from './admin-formula/admin-formula-filter/admin-formula-filter.component';
import { AdminFormulaPointtypeComponent } from './admin-formula/admin-formula-pointtype/admin-formula-pointtype.component';
import { AdminRedeemComponent } from './admin-redeem/admin-redeem.component';
import { AdminRedeemSearchComponent } from './admin-redeem/admin-redeem-search/admin-redeem-search.component';
import { AdminRedeemDetailComponent } from './admin-redeem/admin-redeem-detail/admin-redeem-detail.component';
import { AdminRedeemFilterComponent } from './admin-redeem/admin-redeem-filter/admin-redeem-filter.component';
import { AdminRedeemStorecategoryComponent } from './admin-redeem/admin-redeem-storecategory/admin-redeem-storecategory.component';
import { AdminCustomerComponent } from './admin-customer/admin-customer.component';
import { AdminCustomerSearchComponent } from './admin-customer/admin-customer-search/admin-customer-search.component';
import { AdminCustomerDetailComponent } from './admin-customer/admin-customer-detail/admin-customer-detail.component';
import { AdminCustomerTransactionComponent } from './admin-customer/admin-customer-transaction/admin-customer-transaction.component';
import { AdminCustomerTransMerchantComponent } from './admin-customer/admin-customer-trans-merchant/admin-customer-trans-merchant.component';
import { AdminCustomerTransBankComponent } from './admin-customer/admin-customer-trans-bank/admin-customer-trans-bank.component';
import { AdminCustomerTransEwalletComponent } from './admin-customer/admin-customer-trans-ewallet/admin-customer-trans-ewallet.component';
import { AdminCustomerTransHistoryComponent } from './admin-customer/admin-customer-trans-history/admin-customer-trans-history.component';
import { AdminCustomerTransMembershipcardComponent } from './admin-customer/admin-customer-trans-membershipcard/admin-customer-trans-membershipcard.component';
import { AdminCustomerRedeemComponent } from './admin-customer/admin-customer-redeem/admin-customer-redeem.component';
import { AdminCustomerRedeemFilterComponent } from './admin-customer/admin-customer-redeem-filter/admin-customer-redeem-filter.component';
import { AdminCustomerRedeemSearchComponent } from './admin-customer/admin-customer-redeem-search/admin-customer-redeem-search.component';
import { AdminCustomerRedeemStorecategoryComponent } from './admin-customer/admin-customer-redeem-storecategory/admin-customer-redeem-storecategory.component';
import { AdminCustomerRedeemNotiftransComponent } from './admin-customer/admin-customer-redeem/admin-customer-redeem-notiftrans/admin-customer-redeem-notiftrans.component';
import { AdminPrintersettingComponent } from './admin-printersetting/admin-printersetting.component';
import { AdminProfileChangepasswordComponent } from './admin-profile/admin-profile-changepassword/admin-profile-changepassword.component';
import { AdminProfileChangepasswordNotifComponent } from './admin-profile/admin-profile-changepassword/admin-profile-changepassword-notif/admin-profile-changepassword-notif.component';
import { AdminCustomerTransactionNotifComponent } from './admin-customer/admin-customer-transaction/admin-customer-transaction-notif/admin-customer-transaction-notif.component';

@NgModule({
	declarations: [
		AdminHomeComponent,
		AdminHomeModal1Component,
		AdminFirstPageComponent,
		AdminProfileComponent,
		AdminHistoryComponent,
		AdminModalHistory1Component,
		AdminTestprintComponent,
		AdminFirstPageSwitchsiteComponent,
		AdminMallComponent,
		AdminMallDetailComponent,
		AdminMallFilterComponent,
		AdminMallProvinceComponent,
		AdminMallSearchComponent,
		AdminBrandComponent,
		AdminBrandFilterComponent,
		AdminBrandSearchComponent,
		AdminBrandDetailComponent,
		AdminBrandStorecategoryComponent,
		AdminMerchantComponent,
		AdminMerchantDetailComponent,
		AdminMerchantFilterComponent,
		AdminMerchantSearchComponent,
		AdminMerchantStorecategoryComponent,
		AdminMerchantBrandComponent,
		AdminCardtypeComponent,
		AdminCardtypeSearchComponent,
		AdminCardtypeDetailComponent,
		AdminFormulaComponent,
		AdminFormulaDetailComponent,
		AdminFormulaSearchComponent,
		AdminFormulaFilterComponent,
		AdminFormulaPointtypeComponent,
		AdminRedeemComponent,
		AdminRedeemSearchComponent,
		AdminRedeemDetailComponent,
		AdminRedeemFilterComponent,
		AdminRedeemStorecategoryComponent,
		AdminCustomerComponent,
		AdminCustomerSearchComponent,
		AdminCustomerDetailComponent,
		AdminCustomerTransactionComponent,
		AdminCustomerTransMerchantComponent,
		AdminCustomerTransBankComponent,
		AdminCustomerTransEwalletComponent,
		AdminCustomerTransHistoryComponent,
		AdminCustomerTransMembershipcardComponent,
		AdminCustomerRedeemComponent,
		AdminCustomerRedeemFilterComponent,
		AdminCustomerRedeemSearchComponent,
		AdminCustomerRedeemStorecategoryComponent,
		AdminCustomerRedeemNotiftransComponent,
		AdminPrintersettingComponent,
		AdminProfileChangepasswordComponent,
		AdminProfileChangepasswordNotifComponent,
		AdminCustomerTransactionNotifComponent,
	],
	imports: [
		NativeScriptCommonModule,
		AdminRoutingModule,
		NativeScriptFormsModule
	],
	schemas: [NO_ERRORS_SCHEMA],
	entryComponents: [
		AdminHomeModal1Component,
		AdminModalHistory1Component,
		AdminFirstPageSwitchsiteComponent,
		AdminMallDetailComponent,
		AdminBrandDetailComponent,
		AdminMerchantDetailComponent,
		AdminCardtypeDetailComponent
	]
})
export class AdminModule { }
