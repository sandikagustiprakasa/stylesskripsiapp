import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";

import { AdminHomeModal1Component } from "./modal/admin-home-modal-1.component";

@Component({
	selector: "AdminHome",
	moduleId: module.id,
	templateUrl: "./admin-home.component.html",
	styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {


	public RouteList = [];


	constructor(private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef) {
	}

	ngOnInit(): void {
		this.RouteList = [
			{
				id: 1,
				name: "Route 1",
				status: true
			},
			{
				id: 2,
				name: "Route 2",
				status: false
			},
			{
				id: 3,
				name: "Route 3",
				status: false
			},
			{
				id: 4,
				name: "Route 4",
				status: false
			},
			{
				id: 5,
				name: "Route 5",
				status: false
			},
			{
				id: 6,
				name: "Route 6",
				status: false
			}
		]
	}

	showModal() {
		const options: ModalDialogOptions = {
			viewContainerRef: this.viewContainerRef,
			fullscreen: false,
			context: {}
		};
		this.modalService.showModal(AdminHomeModal1Component, options);
	}



}