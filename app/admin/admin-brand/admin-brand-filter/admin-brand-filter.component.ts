import { Component, OnInit} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"

import { Color } from "tns-core-modules/ui/core/view"

import { GlobalParamBrand } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-brand-filter',
  templateUrl: './admin-brand-filter.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-brand-filter.component.css'
  ]
})
export class AdminBrandFilterComponent implements OnInit {

  public StoreCategoryFilter = {
    ID : null,
    Name : ""
  };


  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _GlobalParamBrand : GlobalParamBrand,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this.StoreCategoryFilter = this._GlobalParamBrand.GetStoreCategory

    this._SP.setUrlBefore('/admin/brand')
  }

  //Take By User------------------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/brand'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoFilter(){
    this.routerExtensions.navigate(['/admin/brand'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToStoreCategoryPage(){

		this.routerExtensions.navigate(['/admin/brand-storecategory'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }


  TimesClick(_Filter){
    if(_Filter == 'storecategory'){
      this.StoreCategoryFilter.ID = null
      this.StoreCategoryFilter.Name = ""
      this._GlobalParamBrand.SetStoreCategory(this.StoreCategoryFilter)
    }

  }


}
