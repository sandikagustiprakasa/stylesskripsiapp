import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { setCurrentOrientation , orientationCleanup} from 'nativescript-screen-orientation';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "tns-core-modules/ui/page"
import { View, Color } from "tns-core-modules/ui/core/view"
import { ScrollView, ScrollEventData } from "tns-core-modules/ui/scroll-view";
import { Animation, AnimationDefinition, CubicBezierAnimationCurve } from "tns-core-modules/ui/animation";
import { SwipeGestureEventData, SwipeDirection, PanGestureEventData, GestureStateTypes } from "tns-core-modules/ui/gestures/gestures";

import * as Dialog from 'ui/dialogs';

import { EventData } from "tns-core-modules/data/observable";
import { isAndroid } from "tns-core-modules/platform";

import { registerElement } from "nativescript-angular/element-registry";
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

import { ServiceProxy } from '../../../shared/service-proxy.service';
import { GlobalParamBrand } from '../../../shared/service-proxy.service';
import { BrandService } from '../../../shared/service-proxy.service';

import { AdminBrandDetailComponent } from "./admin-brand-detail/admin-brand-detail.component";


@Component({
  selector: 'app-admin-brand',
  templateUrl: './admin-brand.component.html',
  styleUrls: [
    '../../assets/local-style.css',
    './admin-brand.component.css'
  ]
})
export class AdminBrandComponent implements OnInit {

  public APIURL = this._SP.APIURL;
  
  public BrandFilter = {
    ID : null,
    Name : ""
  }
  
  //Request Brand List
  public ListRequest = {
    Page: 1,
    PerPage: 10,
    Search : "",
    MasterBrand_ID: null,
    MasterStoreCategory_ID : null,
    MasterProject_ID: this._SP.ProjectID
  }

  //Brand List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion search Brand List
  public Subscribtion = null;

  //information display for Brand List
  public isBrandListLoading = false
  public isBrandListEmpty = true
  public isPullRefresh = null;
  public isLoadMoreItem = false;
  public LoadMoreItemMsg = "";


  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamBrand : GlobalParamBrand,
    private _BrandService : BrandService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {

    this.ListRequest.Search = this._GlobalParamBrand.GetSearch
    this.ListRequest.MasterStoreCategory_ID = this._GlobalParamBrand.GetStoreCategory.ID

    this.GettingListBrandDataAPI(this.ListRequest,false)

    setCurrentOrientation("portrait",function(){
			console.log("portrait orientation");
    });

    this.isLoadMoreItem = false
    this.LoadMoreItemMsg = ""

    this._SP.setUrlBefore('/admin')
   
  }

  //Mobile Code ------------------------------------------------------------------------------------------------
  
  public wrapperLayout : View = undefined;
  public scrollview : ScrollView = undefined;
  public bar : View = undefined;

  public minStrecth = 0;
  public maxStrecth = 40;
  public animationDuration = 250;
  public animation : Animation = undefined;
  public prevTranslateY = 0;
  public prevScale = 0;

  onWrapperLoaded(args : EventData){


    
    // this.wrapperLayout = args.object as View;
    // const page = this.wrapperLayout.page;

    // this.scrollview = page.getViewById("scrollview") as ScrollView;
    // this.bar = page.getViewById("bar") as View;

    // this.bar.eachChildView((v : View) =>{
    //   v.opacity = 0;
    //   return true;
    // });

    // this.bar.translateY = this.bar.translateY - this.maxStrecth;
    // this.bar.height = this.maxStrecth;

    // this.scrollview.marginTop = -1 * this.maxStrecth

    // this.scrollview.on('pan', (args : PanGestureEventData) => {

    //   if(args.state == GestureStateTypes.began){

    //     if(this.animation && this.animation.isPlaying){
    //       this.animation.cancel();
    //     }
      
    //   }else if(args.state == GestureStateTypes.changed){

        
    //     if(args.deltaY < 0){
    //       let newY = this.scrollview.translateY + args.deltaY - this.prevTranslateY;
    //       this.scrollview.translateY = newY;
    //       this.prevTranslateY = args.deltaY;

    //       this.prevScale = this.getScale(this.maxStrecth, newY);
    //       this.bar.eachChildView((v : View) => {
    //         v.opacity = this.prevScale
    //         return true
    //       })

    //       if(newY <= this.maxStrecth && newY >= this.minStrecth){
    //         this.bar.translateY = newY - this.maxStrecth;
    //       }

    //     }

    //   }else if(args.state == GestureStateTypes.ended){
    //     if(args.deltaY < 0){
    //       let transY = this.maxStrecth;
    //       if(args.deltaY < this.maxStrecth / 2){
    //         transY = this.minStrecth
    //       }

    //       const def1 : AnimationDefinition = {
    //         target : this.scrollview,
    //         duration : this.animationDuration,
    //         translate : { x : 0, y : transY},
    //         curve : new CubicBezierAnimationCurve(.18, .52, 0, 1)
    //       }

    //       const def2 : AnimationDefinition = {
    //         target : this.bar,
    //         duration : this.animationDuration,
    //         translate : { x : 0, y : transY - this.maxStrecth},
    //         curve : new CubicBezierAnimationCurve(.18, .52, 0, 1)
    //       }

          
    //       const childDefs : AnimationDefinition[] = [];
    //       this.bar.eachChildView((v : View) => {

    //         const def3 : AnimationDefinition = {
    //           target : v,
    //           duration : this.animationDuration,
    //           opacity : transY == this.maxStrecth? 1 : 0
    //         }
            
    //         childDefs.push(def3);
    //         return true;
    //       })

    //       this.animation = new Animation([def1,def2, ...childDefs]);
    //       this.animation.play();
    //     }
    //   }

    // })

    // this.scrollview.on('swipe', (args : SwipeGestureEventData) => {

    //   if(this.animation && this.animation.isPlaying){
    //     this.animation.cancel();
    //   }

    //   if(args.direction == SwipeDirection.down){

    //     const def1 : AnimationDefinition = {
    //       target : this.bar,
    //       duration : this.animationDuration,
    //       translate : { x : 0, y : this.minStrecth},
    //       curve : new CubicBezierAnimationCurve(.18, .52, 0, 1)
    //     }

    //     const def2 : AnimationDefinition = {
    //       target : this.scrollview,
    //       duration : this.animationDuration,
    //       translate : { x : 0, y : this.maxStrecth},
    //       curve : new CubicBezierAnimationCurve(.18, .52, 0, 1)
    //     }

    //     const childDefs : AnimationDefinition[] = [];
    //     this.bar.eachChildView((v : View) => {

    //       const def3 : AnimationDefinition = {
    //         target : v,
    //         duration : this.animationDuration * 2,
    //         opacity : 1
    //       }
          
    //       childDefs.push(def3);
    //       return true;
    //     })

    //     this.animation = new Animation([def1,def2, ...childDefs]);
    //     this.animation.play();
    //   }

    // })
  }

  getScale(fullDen : number, partNum : number){
    return partNum / fullDen;
  }


  //Take By User------------------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  BackAfterListEmpty(){
    this.GettingListBrandDataAPI(this.ListRequest,false)
  }

  refreshList(args) {
    console.log("Refresh 1")
    this.isPullRefresh = args.object;

    this.ListRequest.Page = 1
    this.ListRequest.PerPage = 10   
    
    this.GettingListBrandDataAPI(this.ListRequest,true)
    // setTimeout(function () {
    //   this.isPullRefresh .refreshing = false;
    //   console.log("Refresh 2")
    // }, 1000);
  }

  loadMoreItems(args: ScrollEventData) {
    const scrollView = args.object as ScrollView;

    // console.log("Height: " + scrollView.scrollableHeight);
    // console.log("scrollX: " + args.scrollX);
    // console.log("scrollY: " + args.scrollY);
    
    if(scrollView.scrollableHeight == args.scrollY){
      if(this.ListRequest.Page < Math.ceil(this.ListDataPaging.count / this.ListRequest.PerPage)){
        this.ListRequest.Page = this.ListRequest.Page + 1 
        this.GettingListBrandDataLoadMoreItemAPI(this.ListRequest)
      }
    }
  }

  ViewDetail(_i){
    const options: ModalDialogOptions = {
			viewContainerRef: this.viewContainerRef,
			fullscreen: false,
			animated : true,
			context: {
        Detail : this.ListData[_i]
      }
		};
		this.modalService.showModal(AdminBrandDetailComponent, options).then(result => {
			// result value is the string you attach in closeCallBack(‘cute’)	
		})
		.catch(error => {
			// const options : Dialog.AlertOptions = {
			// 	title : "OH Error!",
			// 	message : "No Response",
			// 	okButtonText : 'Okay'
			// }
			// Dialog.alert(options).then(()=>{
			// })
		});
  }

  GoToBrandFilterPage(){

    
		this.routerExtensions.navigate(['/admin/brand-filter'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }
  
  
  GoToBrandSearchPage(){

    
		this.routerExtensions.navigate(['/admin/brand-search'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  TimesClick(){
    this._GlobalParamBrand.SetSearch("")
    this.ListRequest.Search = this._GlobalParamBrand.GetSearch

    this.GettingListBrandDataAPI(this.ListRequest,false)
  }
  
  

  //Funtion Engine to API---------------------------------------------------------------------------------------
  GettingListBrandDataAPI(_Request,_isPull){
    this.ListData = []
    this.isBrandListLoading = true
    this.isBrandListEmpty = false
    this.Subscribtion = this._BrandService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          //console.log(JSON.stringify(result))

          if(result.Data != null){
            
            this.ListData = result.Data

            this.ListDataPaging.count = result.TotalData

          }else{
            this.ListData = []
            this.isBrandListEmpty = true
          }
        }else{
          this.ListData = []
          this.isBrandListEmpty = true
        }
        
        this.isBrandListLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
					title : "OH!",
					message : JSON.stringify(err.error),
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
        })
        
        this.isBrandListLoading = false
        this.isBrandListEmpty = true

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
          console.log("Refresh 2")
        }

        this._cRef.detectChanges();
      });
  }

  GettingListBrandDataLoadMoreItemAPI(_Request){
    this.isLoadMoreItem = true
    this.LoadMoreItemMsg = "Load Data.."
    this.Subscribtion = this._BrandService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          
          if(result.Data != null){
            //console.log("More Item", JSON.stringify(result))
            this.ListData = this.ListData.concat(result.Data)

          }else{
            this.LoadMoreItemMsg = "You have reach all data"
          }
        }else{
          this.LoadMoreItemMsg = "There is some error, pleas refresh and try again"
        }
        
        this.isLoadMoreItem = false
        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
					title : "OH!",
					message : "Load More item : " + JSON.stringify(err.error),
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
        })

        this.LoadMoreItemMsg = "Load More item : " + JSON.stringify(err.error)
        this.isLoadMoreItem = false

        this._cRef.detectChanges();
      });
  }

}
