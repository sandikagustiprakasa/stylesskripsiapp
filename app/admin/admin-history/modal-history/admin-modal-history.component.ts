import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

@Component({
    selector: "AdminModalHistory1",
    moduleId: module.id,
    templateUrl: "./admin-modal-history.component.html",
    styleUrls: ['./admin-modal-history.component.css']
})
export class AdminModalHistory1Component implements OnInit {

    public Routelist = [];

    // constructor(private params: ModalDialogParams) { }

    constructor(private params: ModalDialogParams) { }

    ngOnInit(): void {
    }


    close(): void {
        this.params.closeCallback();
    }

}