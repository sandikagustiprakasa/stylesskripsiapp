import { Component, OnInit} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"
import { EventData } from "tns-core-modules/data/observable";
import { View, Color } from "tns-core-modules/ui/core/view"
import { Switch } from "tns-core-modules/ui/switch";

import { GlobalParamRedeem } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-redeem-filter',
  templateUrl: './admin-redeem-filter.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-redeem-filter.component.css'
  ]
})
export class AdminRedeemFilterComponent implements OnInit {

  public StoreCategoryFilter = {
    ID : null,
    Name : ""
  };


  public IsActiveMsg = ""
  public IsActiveFilterSingle = null


  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _GlobalParamRedeem : GlobalParamRedeem,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this.StoreCategoryFilter = this._GlobalParamRedeem.GetStoreCategory
  }
  //Mobile Code ------------------------------------------------------------------------------------------------
  public wrapperLayout : View = undefined;
  public SwitchActive : Switch = undefined

  onWrapperLoaded(args : EventData){
    this.wrapperLayout = args.object as View;
    const page = this.wrapperLayout.page;

    this.SwitchActive = page.getViewById("ActiveSwitch") as Switch;
    this.SwitchActive.checked = this._GlobalParamRedeem.GetIsActive;

    this.IsActiveMsg = this._GlobalParamRedeem.GetIsActive ? 'Active' : 'Inactive';
    this.IsActiveFilterSingle = this._GlobalParamRedeem.GetIsActive;
  }

  //Take By User------------------------------------------------------------------------------------------------
 
  onCheckedChange(args: EventData) {
    let sw = args.object as Switch;
    this.SwitchActive.checked = sw.checked; // boolean
    this._GlobalParamRedeem.SetIsActive(sw.checked);

    this.IsActiveMsg = this._GlobalParamRedeem.GetIsActive ? 'Active' : 'Inactive';
    this.IsActiveFilterSingle = this._GlobalParamRedeem.GetIsActive;

    console.log(this.IsActiveFilterSingle)
  }

  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/redeem'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoFilter(){
    this.routerExtensions.navigate(['/admin/redeem'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToStoreCategoryPage(){

		this.routerExtensions.navigate(['/admin/redeem-storecategory'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }


  TimesClick(_Filter){
    if(_Filter == 'StoreCategory'){
      this.StoreCategoryFilter.ID = null
      this.StoreCategoryFilter.Name = ""
      this._GlobalParamRedeem.SetStoreCategory(this.StoreCategoryFilter)
    }
  }


}
