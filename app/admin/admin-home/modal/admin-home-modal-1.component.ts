import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

@Component({
	selector: "AdminHomeModal1",
	moduleId: module.id,
	templateUrl: "./admin-home-modal-1.component.html",
	styleUrls: ['./admin-home-modal-1.component.css']
})
export class AdminHomeModal1Component implements OnInit {

	constructor(private params: ModalDialogParams) { }

	ngOnInit(): void {
	}

	close(): void {
		this.params.closeCallback();
	}
}