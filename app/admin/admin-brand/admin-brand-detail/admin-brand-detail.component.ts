import { Component, OnInit } from '@angular/core';
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import * as ApplicationSettings from "application-settings";

import { Color } from "tns-core-modules/ui/core/view"

import { Page } from "ui/page"
import * as application from "tns-core-modules/application";

import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-brand-detail',
  templateUrl: './admin-brand-detail.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-brand-detail.component.css'
  ]
})
export class AdminBrandDetailComponent implements OnInit {

  public APIURL = this._SP.APIURL;

  public DetailData = {}

  constructor(
    private page: Page,
    private params: ModalDialogParams,
    private _SP : ServiceProxy,) { 
      setTimeout(function(){ 
      
        page.statusBarStyle = "light";
        page.androidStatusBarBackground = new Color("#F5F5F5");
  
       }, 500);
    }

  ngOnInit() {
    this.DetailData = this.params.context.Detail
  }

  CloseClick(){
    this.params.closeCallback()
  }

}
