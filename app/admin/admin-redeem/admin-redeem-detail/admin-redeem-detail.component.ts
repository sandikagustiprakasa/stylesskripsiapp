import { Component, OnInit, ViewContainerRef, ChangeDetectorRef, ViewChildren, ElementRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import { TextField } from "tns-core-modules/ui/text-field"
import * as Dialog from 'ui/dialogs';

import { View, Color } from "tns-core-modules/ui/core/view"
import { EventData } from "tns-core-modules/data/observable";

import { GlobalParamRedeem } from '../../../../shared/service-proxy.service';
import { RedeemService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';


@Component({
  selector: 'app-admin-redeem-detail',
  templateUrl: './admin-redeem-detail.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-redeem-detail.component.css'
  ]
})
export class AdminRedeemDetailComponent implements OnInit {

  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamRedeem : GlobalParamRedeem,
    private _RedeemService :RedeemService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
        
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

      }, 500);
  }

  //Request Redeem Detail
  public DetailRequest = {
    Page: 1,
    PerPage: 10,
    Search : "",
    IsActive : null,
    MasterRedeem_ID : null,
    MasterStoreCategory_ID : null,
    MasterProject_ID: this._SP.ProjectID,
  }

  //Redeem Detail Data & Paging 
  public DetailData = []
  public AppliesToMsg = ""
  

  //For abort subscribtion search Redeem Detail
  public Subscribtion = null;

  //information display for Redeem Detail
  public isRedeemDetailLoading = false
  public isRedeemDetailEmpty = true
  public isPullRefresh = null;

  ngOnInit() {
    this.DetailRequest.MasterRedeem_ID = this._GlobalParamRedeem.GetRedeemID
    this.GettingDetailRedeemDataAPI(this.DetailRequest,false)

    this._SP.setUrlBefore('/admin/redeem')
  }

  //Take By User------------------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/redeem'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  refreshDetail(args) {
    this.isPullRefresh = args.object;

    this.GettingDetailRedeemDataAPI(this.DetailRequest, true)
  }

  //Api Function ------------------------------------------------------------------------------------------------
  GettingDetailRedeemDataAPI(_Request,_isPull){
    this.DetailData = []
    this.isRedeemDetailLoading = true
    this.isRedeemDetailEmpty = false
    this.Subscribtion = this._RedeemService.Detail(_Request)
      .subscribe((result: any) => {
        if(result != null){

          if(result.Data != null){
            
            this.DetailData = result.Data

            if(result.Data.MasterRedeem_AppliesToCode == 1){
              this.AppliesToMsg = "Malls"
            }

            if(result.Data.MasterRedeem_AppliesToCode == 2){
              this.AppliesToMsg = "Merchants"
            }

          }else{
            this.DetailData = []
            this.isRedeemDetailEmpty = true
          }
        }else{
          this.DetailData = []
          this.isRedeemDetailEmpty = true
        }
        
        this.isRedeemDetailLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
          title : "OH!",
          message : JSON.stringify(err.error),
          okButtonText : 'Okay'
        }
        Dialog.alert(options).then(()=>{
        })
        
        this.isRedeemDetailLoading = false
        this.isRedeemDetailEmpty = true

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      });
  }

}
