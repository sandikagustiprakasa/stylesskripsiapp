import { Component, OnInit } from '@angular/core';
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import * as ApplicationSettings from "application-settings";

import { Page } from "ui/page"
import * as application from "tns-core-modules/application";
import {isAndroid} from "tns-core-modules/platform";

import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-first-page-switchsite',
  templateUrl: './admin-first-page-switchsite.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-first-page-switchsite.component.css'
  ]
})
export class AdminFirstPageSwitchsiteComponent implements OnInit {

  public items = [
    {
      ID : "",
      Name : "Retrieving Item...",
      Picture : ""
    }
  ]

  constructor(
      private page: Page,
      private params: ModalDialogParams,
      private _SP : ServiceProxy,
    ) {

      //Modal Cannot Close when Tap Any Background
      this.page.on("loaded", data => {
        if (isAndroid) {
          let fragmentManger = application.android.foregroundActivity.getFragmentManager();
          let dialogFragment = fragmentManger.findFragmentByTag("dialog");
          if (dialogFragment) {
            dialogFragment.setCancelable(false);
          }
        }
    
      });

  }


  ngOnInit() {
    this.items = [];
    var Sites = JSON.parse(ApplicationSettings.getString('StorageUserLog')).AppSite
    for(var i = 0; i < Sites.length; i++){
      if(Sites[i].MasterSite_ID != this._SP.UserSiteData.ID){
        this.items.push({
          ID : Sites[i].MasterSite_ID,
          Name : Sites[i].MasterSite_Name,
          Picture : Sites[i].MasterSite_Picture
        })
      }
    }

  }

  onItemTap(args: ItemEventData){
    //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);
    this.params.closeCallback(this.items[args.index])
  }

  // close(): void {
  //   this.params.closeCallback();
  // }

}
