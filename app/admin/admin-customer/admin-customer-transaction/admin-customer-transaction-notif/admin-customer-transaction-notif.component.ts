import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { setCurrentOrientation , orientationCleanup} from 'nativescript-screen-orientation';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "tns-core-modules/ui/page"
import { View, Color } from "tns-core-modules/ui/core/view"
import { ScrollView, ScrollEventData } from "tns-core-modules/ui/scroll-view";
import { Animation, AnimationDefinition, CubicBezierAnimationCurve } from "tns-core-modules/ui/animation";
import { SwipeGestureEventData, SwipeDirection, PanGestureEventData, GestureStateTypes } from "tns-core-modules/ui/gestures/gestures";

import * as Dialog from 'ui/dialogs';

import { EventData } from "tns-core-modules/data/observable";
import { isAndroid } from "tns-core-modules/platform";

import { registerElement } from "nativescript-angular/element-registry";
registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);

import { GlobalParamCustomerEarning } from '../../../../../shared/service-proxy.service';
import { GlobalParamPrinterSetting } from '../../../../../shared/service-proxy.service';
import { CustomerTransactionHistoryService } from '../../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../../shared/service-proxy.service';
import { Hprt, HPRTPrinter } from "nativescript-hprt";
import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";

import * as $ from 'jquery';
const AratakaConfig = require("~/assets/AratakaConfig.js").AratakaConfig;
@Component({
  selector: 'app-admin-customer-transaction-notif',
  templateUrl: './admin-customer-transaction-notif.component.html',
  styleUrls: [
    '../../../../assets/local-style.css',
    './admin-customer-transaction-notif.component.css'
  ]
})
export class AdminCustomerTransactionNotifComponent implements OnInit {

  private hprt: Hprt;
  public isBluetoothEnabled : boolean = false;
  loader: any; 

  public isTransaction : boolean = false
  public msg : string = ""

  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _GlobalParamCustomerEarning : GlobalParamCustomerEarning,
    private _GlobalParamPrinterSetting :GlobalParamPrinterSetting,
    private _CustomerTransactionHistoryService : CustomerTransactionHistoryService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);

     
    this.hprt = new Hprt()
    this.isBluetoothEnabled = this.hprt.isBluetoothEnabled();
    this.loader = new LoadingIndicator();
  }

  ngOnInit() {
   this.isTransaction = this._GlobalParamCustomerEarning.GetIsTransactionSuccess
   this.msg = this._GlobalParamCustomerEarning.GetTransactiomMsg

   this._SP.setUrlBefore('/admin/customer-detail')
  }

  PrintReceipt(){
    if(!this.isBluetoothEnabled){
      this.GoToPrintterSetting()
    }else{

      if(this._GlobalParamCustomerEarning.GetCustomerTransactionHistoryID != null){

        if(this._GlobalParamPrinterSetting.GetIsSelectedPrinter){
          this.GettingListCustomerTransHistoryDataAPI({
            Page: null,
            PerPage: null,
            CustomerTransactionHistory_ID : this._GlobalParamCustomerEarning.GetCustomerTransactionHistoryID,
            MasterCustomerData_ID : null,
            MasterMerchant_ID : null,
            MasterSite_ID : null,
            MasterProject_ID: null,
            IsConfirmation : null,
            IsActive : null
          })
        }else{
          this.GoToPrintterSetting()
        }
        
      }

    }

  }

  GoToPrintterSetting(){
    this._GlobalParamPrinterSetting.SetActionFrom("app-admin-customer-transaction-notif")
    this.routerExtensions.navigate(['/admin/printersetting'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  BackToPageBefore(isFailed){
    var address = ""
    if(!isFailed){
      address = '/admin/customer-detail'
    }else{
      address = '/admin/customer-transaction'
    }
    this.routerExtensions.navigate([address],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  PrintReceiptData(_data) {
    // Sum this numbers to get attribute combination
    // Example: 
    // Double height: 16
    // Double Width: 32
    // Underline: 4
    // Bold: 2
    // Mini: 1
    // White text: 8

    var dateNow = new Date();

    this.hprt.newLine();
    this.hprt.printTextCenter("LIPPO MALLS INDONESIA");
    this.hprt.printTextCenter("----------");
    this.hprt.newLine();
    this.hprt.printTextCenter("Jl. Boulevard Gajah Mada No.2121, RT.001/RW.009, Panunggangan Bar., Kec. Cibodas, Kota Tangerang, Banten 15810");
    this.hprt.printTextCenter("Kota Tangerang, Indonesia");
    this.hprt.printTextCenter("Twitter: @lippomalls");
    this.hprt.newLine();
    this.hprt.printTextSimple("Date : "+ AratakaConfig.formatDateYMDHS(dateNow));
    this.hprt.newLine();
    
    if(_data.MasterRedeem_ID != null){
      this.hprt.printTextMini("Item : "+ _data.MasterRedeem_Name);    
      this.hprt.horizontalLine();       
    }
    if(_data.MasterMerchant_ID != null){
      this.hprt.printTextMini("Merchant : "+ _data.MasterMerchant_Name);    
      this.hprt.horizontalLine();       
    }
    this.hprt.printTextMini("Earning Type : "+ _data.MasterEarningType_Name);    
    this.hprt.horizontalLine();       
    this.hprt.printTextMini("Card Type : "+ _data.CustomerDataMemberCardType_Name); 
    this.hprt.horizontalLine();       
    this.hprt.printTextMini( "Amount : Rp. " + AratakaConfig.formatRupiah(_data.CustomerTransactionHistory_Amount).toString());    
    this.hprt.horizontalLine();     
    this.hprt.printTextMini( "Mall : "+ _data.MasterSite_Name);    
    this.hprt.horizontalLine();    
    if(_data.CustomerTransactionHistory_LuckyDrawPoint != null){
      this.hprt.printTextMini("L Draw Point : "+ _data.CustomerTransactionHistory_LuckyDrawPoint.toString());    
      this.hprt.horizontalLine();       
    }    
    this.hprt.printTextMini( "Redeem Point : "+ _data.CustomerTransactionHistory_RedeemPoint.toString());    
    this.hprt.horizontalLine();
    if(_data.MasterTransactionType_ID != null){
      this.hprt.printTextMini( "Transaction : "+ _data.MasterTransactionType_Name);    
      this.hprt.horizontalLine();       
    }    
    if(_data.MasterBank_ID != null){
      this.hprt.printTextMini( "Bank : "+ _data.MasterBank_Name);    
      this.hprt.horizontalLine();       
    }    
    if(_data.MasterEWallet_ID != null){
      this.hprt.printTextMini( "E-Wallet : "+ _data.MasterEWallet_Name);    
      this.hprt.horizontalLine();       
    }    
    // for (let index = 0; index < cart.length; index++) {
    //     this.hprt.printTextSimple(cart[index]);    
    //     this.hprt.horizontalLine();        
    // }
    this.hprt.newLine(4);
  }

  GettingListCustomerTransHistoryDataAPI(_Request){
    //this.loader.show();
    this._CustomerTransactionHistoryService.List(_Request)
      .subscribe((result: any) => {
        if(result.Data != null){
            
          this.PrintReceiptData(result.Data[0])

        }else{
          const options : Dialog.AlertOptions = {
            title : "OH!",
            message : "There is somthing Error!",
            okButtonText : 'Okay'
          }
          Dialog.alert(options).then(()=>{
          })
        }

        this.loader.hide()
 
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
					title : "OH!",
					message : JSON.stringify(err.error),
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
        })
        
        this.loader.hide()
 

      });
  }
  

}
