import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";

import { AdminModalHistory1Component } from "./modal-history/admin-modal-history.component";

@Component({
	selector: "AdminHistory",
	moduleId: module.id,
	templateUrl: "./admin-history.component.html",
	styleUrls: ['./admin-history.component.css']
})
export class AdminHistoryComponent implements OnInit {

	public RouteList = [];

	constructor(private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef) {
	}

	ngOnInit(): void {
		this.RouteList = [
			{
				id: 1,
				name: "Route 1",
				detail: "Ground Floor"
			}
		]
	}

	showModal() {
		const options: ModalDialogOptions = {
			viewContainerRef: this.viewContainerRef,
			fullscreen: false,
			context: {}
		};
		this.modalService.showModal(AdminModalHistory1Component, options);

	}

}