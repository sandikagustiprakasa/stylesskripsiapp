import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import * as Dialog from 'ui/dialogs';
import { Color } from "tns-core-modules/ui/core/view"

import { AuthService } from '../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../shared/service-proxy.service';

@Component({
  selector: 'app-auth-forgotpassword',
  templateUrl: './auth-forgotpassword.component.html',
  styleUrls: [
    '../../assets/local-style.css',
    './auth-forgotpassword.component.css'
  ]
})
export class AuthForgotpasswordComponent implements OnInit {


  public SendLoading = false
  public Msg : string = ""

  //Model
  public Email: string = "";
  
  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _AuthService : AuthService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "dark";
      page.androidStatusBarBackground = new Color("#004D40");
     }, 500);

  }


  ngOnInit() {
  }

  //Take Action --------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/auth'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  
  ForgotPasswordSubmit(){
    // var d1 = new Date();
    // var d2 = (new Date(d1)).setMinutes(d1.getMinutes() + 10);
    // +AratakaConfig.EncryptoJS(d1.toString() + "-" + new Date(d2).toString())

    if(this.Email != ''){
      this.ForgotPasswordAPI({
        Email : this.Email,
        LinkAddress : "http://192.168.43.168:81/skripsi/auth/passwordrecovery/"
      })
    }
  }

	ForgotPasswordAPI(_Request){
		this.SendLoading = true
    this.Msg = ""
		this._AuthService.ForgotPassword(_Request)
		.subscribe((result: any) => {
			console.log(JSON.stringify(result));
      
      if (result.Code == 1) {
        this.Email = ""
        this.Msg = result.Message
			}else{
        this.Msg = result.Message
			}

      var that = this;
      setTimeout(function(){ that.Msg = "" }, 3000);
      this.SendLoading = false  
		  },
		  (err: HttpErrorResponse) => {
			  console.log(JSON.stringify(err));
			  alert(JSON.stringify(err))
        this.SendLoading = false
		  });
	
	}
	
  

  
}
