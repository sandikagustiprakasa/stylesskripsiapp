import { Component, OnInit} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"
import { EventData } from "tns-core-modules/data/observable";
import { View, Color } from "tns-core-modules/ui/core/view"
import { Switch } from "tns-core-modules/ui/switch";

import { GlobalParamCustomerRedeem } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-customer-redeem-filter',
  templateUrl: './admin-customer-redeem-filter.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-customer-redeem-filter.component.css'
  ]
})
export class AdminCustomerRedeemFilterComponent implements OnInit {

  public StoreCategoryFilter = {
    ID : null,
    Name : ""
  };


  public IsActiveMsg = ""
  public IsActiveFilterSingle = null


  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _GlobalParamCustomerRedeem : GlobalParamCustomerRedeem,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this.StoreCategoryFilter = this._GlobalParamCustomerRedeem.GetStoreCategory
    this._SP.setUrlBefore('/admin/customer-redeem')
  }
  //Mobile Code ------------------------------------------------------------------------------------------------
  public wrapperLayout : View = undefined;
  public SwitchActive : Switch = undefined

  onWrapperLoaded(args : EventData){
    this.wrapperLayout = args.object as View;
    const page = this.wrapperLayout.page;

    this.SwitchActive = page.getViewById("ActiveSwitch") as Switch;
    this.SwitchActive.checked = this._GlobalParamCustomerRedeem.GetIsActive;

    this.IsActiveMsg = this._GlobalParamCustomerRedeem.GetIsActive ? 'Active' : 'Inactive';
    this.IsActiveFilterSingle = this._GlobalParamCustomerRedeem.GetIsActive;
  }

  //Take By User------------------------------------------------------------------------------------------------
 
  onCheckedChange(args: EventData) {
    let sw = args.object as Switch;
    this.SwitchActive.checked = sw.checked; // boolean
    this._GlobalParamCustomerRedeem.SetIsActive(sw.checked);

    this.IsActiveMsg = this._GlobalParamCustomerRedeem.GetIsActive ? 'Active' : 'Inactive';
    this.IsActiveFilterSingle = this._GlobalParamCustomerRedeem.GetIsActive;

    console.log(this.IsActiveFilterSingle)
  }

  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/customer-redeem'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoFilter(){
    this.routerExtensions.navigate(['/admin/customer-redeem'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToStoreCategoryPage(){

		this.routerExtensions.navigate(['/admin/customer-redeem-storecategory'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }


  TimesClick(_Filter){
    if(_Filter == 'StoreCategory'){
      this.StoreCategoryFilter.ID = null
      this.StoreCategoryFilter.Name = ""
      this._GlobalParamCustomerRedeem.SetStoreCategory(this.StoreCategoryFilter)
    }
  }

}
