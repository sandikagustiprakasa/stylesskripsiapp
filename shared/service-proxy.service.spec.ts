import { TestBed } from '@angular/core/testing';

import { ServiceProxy } from './service-proxy.service';

describe('ServiceProxyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceProxy = TestBed.get(ServiceProxy);
    expect(service).toBeTruthy();
  });
});
