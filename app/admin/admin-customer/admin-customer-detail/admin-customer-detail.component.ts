import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import * as Dialog from 'ui/dialogs';

import { Color } from "tns-core-modules/ui/core/view"

import { GlobalParamCustomer } from '../../../../shared/service-proxy.service';
import { GlobalParamTransaction } from '../../../../shared/service-proxy.service';
import { CustomerService } from '../../../../shared/service-proxy.service';
import { CustomerTransactionHistoryService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

import * as camera from "nativescript-camera";
import { Image } from "tns-core-modules/ui/image";

import * as $ from 'jquery';
//declare const AratakaConfig : any;
const AratakaConfig = require("~/assets/AratakaConfig.js").AratakaConfig;


@Component({
  selector: 'app-admin-customer-detail',
  templateUrl: './admin-customer-detail.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-customer-detail.component.css'
  ]
})
export class AdminCustomerDetailComponent implements OnInit {
  
  public APIURL = this._SP.APIURL;

  public CustomerPoint = {
    CustomerTransactionHistory_Amount : "",
    CustomerTransactionHistory_RedeemPoint : "",
    CustomerTransactionHistory_LuckyDrawPoint : "",
    CustomerTransactionHistory_TotalAmount : "",
    CustomerRedeemPoint_TotalRedeemPoint : "",
    CustomerRedeemPoint_TotalLuckyDrawPoint : ""
  }

  //Request CustomerDetail List
  public ListRequest = {
    Page: null,
    PerPage: null,
    Search : "",
    MasterCustomerData_ID: null,
    MasterProject_ID: this._SP.ProjectID,
    Status : null,
  }

  public ListMenu = [
    {
      ID : 1,
      Name : "Transaction History"
    },
    {
      ID : 2,
      Name : "Card Type History"
    }
  ]

  public CardType = {
    CustomerDataMemberCardType_ID : null,
    ID : null,
    Name : ""
  }

  //CustomerDetail List Data & Paging 
  public DetailData = []
  public DetailDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchCustomerDetail List
  public Subscribtion = null;

  //information display forCustomerDetail List
  public isCustomerDetailListLoading = false
  public isCustomerDetailListEmpty = true
  public isPullRefresh = null;

  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamCustomer : GlobalParamCustomer,
    private _GlobalParamTransaction : GlobalParamTransaction,
    private _CustomerService :CustomerService,
    private _CustomerTransactionHistoryService : CustomerTransactionHistoryService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this.ListRequest.MasterCustomerData_ID = this._GlobalParamCustomer.GetCustomerID;
    this.GettingListCustomerDetailDataAPI(this.ListRequest,false)

    this._SP.setUrlBefore('/admin/customer')
  }
  //Take By User------------------------------------------------------------------------------------------------
  refreshList(args) {
    console.log("Refresh 1")
    this.isPullRefresh = args.object;

    
    this.GettingListCustomerDetailDataAPI(this.ListRequest,true)
    // setTimeout(function () {
    //   this.isPullRefresh .refreshing = false;
    //   console.log("Refresh 2")
    // }, 1000);
  }

  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/customer'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }



  BackAfterListEmpty(){
    this.GettingListCustomerDetailDataAPI(this.ListRequest,false)
  }

  CaptureReceipt(){

    this._GlobalParamTransaction.SetBank({
      ID : null,
      Name : ""
    })
    this._GlobalParamTransaction.SetEWallet({
      ID : null,
      Name : ""
    })
    this._GlobalParamTransaction.SetMerchant({
      ID : null,
      Name : "",
      StoreCategoryID : null,
      StoreCategoryName : ""
    })

    this._GlobalParamTransaction.SetAmount(null)
    this._GlobalParamTransaction.SetReceiptNumber("")
    this._GlobalParamTransaction.SetTransactionID(null)

    this._GlobalParamTransaction.SetSite({
      ID : (this._SP.UserSiteData.ID != null)? [this._SP.UserSiteData.ID] : [],
      Name : (this._SP.UserSiteData.Name != null)? [this._SP.UserSiteData.Name] : null,
    })

    this._GlobalParamTransaction.SetCustomerID(this._GlobalParamCustomer.GetCustomerID)

    this._GlobalParamTransaction.SetCardType({
      CustomerDataMemberCardType_ID : this.CardType.CustomerDataMemberCardType_ID,
      ID : this.CardType.ID,
      Name : this.CardType.Name
    })

    const options = {
        width: 300,
        height: 300,
        keepAspectRatio: false,
        saveToGallery: false
    };

    var th = this;
    camera.requestPermissions().then(
        function success() {
          camera.takePicture(options)
            .then((imageAsset) => {
              //console.log(JSON.stringify(imageAsset));
                var image = new Image();
                image.src = imageAsset;

                setTimeout(function(){
                  th._GlobalParamTransaction.SetReceipt(image)
                  th.routerExtensions.navigate(['/admin/customer-transaction'],
                    {
                      clearHistory: true,
                      animated: true,
                      transition: {
                        name: "slideLeft",
                        duration: 300,
                        curve: "easeIn"
                      }
                    }
                  );
                },500)

            }).catch((err) => {
                //alert("Error -> " + err.message);
            });
        }, 
        function failure() {
          const options : Dialog.AlertOptions = {
            title : "OH!",
            message : "Failed to Access Permission",
            okButtonText : 'Okay'
          }
          Dialog.alert(options).then(()=>{
          })
        }
    );

    
  }


  RedeemReward(){
    this.routerExtensions.navigate(['/admin/customer-redeem'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideLeft",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  GoToTransDetail(_i){
    //console.log(_i)
    if(this.ListMenu[_i].ID == 1){
      this.routerExtensions.navigate(['/admin/customer-trans-history'],
        {
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideLeft",
            duration: 300,
            curve: "easeIn"
          }
        }
      );
    }

    if(this.ListMenu[_i].ID == 2){
      this.routerExtensions.navigate(['/admin/customer-trans-membershipcard'],
        {
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideLeft",
            duration: 300,
            curve: "easeIn"
          }
        }
      );
    }
  }


  //Funtion Engine to API---------------------------------------------------------------------------------------
  GettingListCustomerDetailDataAPI(_Request,_isPull){
    this.DetailData = []
    this.isCustomerDetailListLoading = true
    this.isCustomerDetailListEmpty = false
    this.Subscribtion = this._CustomerService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          //console.log(JSON.stringify(result))

          if(result.Data != null){
            
            if(result.Data[0].MasterCustomerData_Gender == 1){
              result.Data[0].MasterCustomerData_Gender = "Male"
            }else if(result.Data[0].MasterCustomerData_Gender == 2){
              result.Data[0].MasterCustomerData_Gender = "Female"
            }else{
              result.Data[0].MasterCustomerData_Gender = "Other"
            }

            if(result.Data[0].MasterCustomerData_MaritalStatus == 1){
              result.Data[0].MasterCustomerData_MaritalStatus = "Married"
            }else{
              result.Data[0].MasterCustomerData_MaritalStatus = "Singel"
            }

            this.DetailData = result.Data[0]

            this.CardType = {
              CustomerDataMemberCardType_ID : result.Data[0].CardType.CustomerDataMemberCardType_ID,
              ID : result.Data[0].CardType.MasterMemberCardType_ID,
              Name : result.Data[0].CardType.MasterMemberCardType_Name
            }
   
            this._GlobalParamCustomer.SetMembershipCardTypeID(result.Data[0].CardType.MasterMemberCardType_ID)

            var my_date = new Date(); 
            var first_date = new Date(my_date.getFullYear(), my_date.getMonth(), 1);
            var last_date = new Date(my_date.getFullYear(), my_date.getMonth() + 1, 0); 
            // console.log(AratakaConfig.formatDateYMD(first_date))
            // console.log(last_date)
            this.GettingCustomerPointDataAPI({
              MasterCustomerData_ID : result.Data[0].MasterCustomerData_ID,
              MasterProject_ID : this._SP.ProjectID,
              FirstDateMonth: AratakaConfig.formatDateYMD(first_date) + ' 00:00',
              LastDateMonth:  AratakaConfig.formatDateYMD(last_date) + ' 23:59:59'
            })

          }else{
            this.DetailData = []
            this.isCustomerDetailListEmpty = true
          }
        }else{
          this.DetailData = []
          this.isCustomerDetailListEmpty = true
        }
        
        this.isCustomerDetailListLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
          title : "OH!",
          message : JSON.stringify(err.error),
          okButtonText : 'Okay'
        }
        Dialog.alert(options).then(()=>{
        })
        
        this.isCustomerDetailListLoading = false
        this.isCustomerDetailListEmpty = true

        this._cRef.detectChanges();
      });
  }

  GettingCustomerPointDataAPI(_Request){
    this.CustomerPoint = {
      CustomerTransactionHistory_Amount : "0",
      CustomerTransactionHistory_RedeemPoint : "0",
      CustomerTransactionHistory_LuckyDrawPoint : "0",
      CustomerTransactionHistory_TotalAmount : "0",
      CustomerRedeemPoint_TotalRedeemPoint : "0",
      CustomerRedeemPoint_TotalLuckyDrawPoint : "0"
    }
    this._CustomerTransactionHistoryService.CustomerPoint(_Request)
      .subscribe((result: any) => {
        if(result != null){
          
          this._GlobalParamCustomer.SetCustomerTotalPoint(0)

          if(result.Data != null){

            this.CustomerPoint = {
              CustomerTransactionHistory_Amount : (result.Data.CustomerTransactionHistory_Amount != null)? result.Data.CustomerTransactionHistory_Amount : '0',
              CustomerTransactionHistory_RedeemPoint :(result.Data.CustomerTransactionHistory_RedeemPoint != null)? result.Data.CustomerTransactionHistory_RedeemPoint : '0',
              CustomerTransactionHistory_LuckyDrawPoint : (result.Data.CustomerTransactionHistory_LuckyDrawPoint != null)? result.Data.CustomerTransactionHistory_LuckyDrawPoint : '0',
              CustomerTransactionHistory_TotalAmount : (result.Data.CustomerTransactionHistory_TotalAmount != null)? result.Data.CustomerTransactionHistory_TotalAmount : '0',
              CustomerRedeemPoint_TotalRedeemPoint : (result.Data.CustomerRedeemPoint_TotalRedeemPoint != null)? result.Data.CustomerRedeemPoint_TotalRedeemPoint : '0',
              CustomerRedeemPoint_TotalLuckyDrawPoint : (result.Data.CustomerRedeemPoint_TotalLuckyDrawPoint != null)? result.Data.CustomerRedeemPoint_TotalLuckyDrawPoint : '0'
            }

            this._GlobalParamCustomer.SetCustomerTotalPoint(this.CustomerPoint.CustomerRedeemPoint_TotalRedeemPoint)

          }else{
            //console.log('halo')
            this.CustomerPoint = {
              CustomerTransactionHistory_Amount : "0",
              CustomerTransactionHistory_RedeemPoint : "0",
              CustomerTransactionHistory_LuckyDrawPoint : "0",
              CustomerTransactionHistory_TotalAmount : "0",
              CustomerRedeemPoint_TotalRedeemPoint : "0",
              CustomerRedeemPoint_TotalLuckyDrawPoint : "0"
            }
          }
        }else{

          this.CustomerPoint = {
            CustomerTransactionHistory_Amount : result.Message,
            CustomerTransactionHistory_RedeemPoint : result.Message,
            CustomerTransactionHistory_LuckyDrawPoint : result.Message,
            CustomerTransactionHistory_TotalAmount :  result.Message,
            CustomerRedeemPoint_TotalRedeemPoint : result.Message,
            CustomerRedeemPoint_TotalLuckyDrawPoint : result.Message
          }
        }


      },
      (err: HttpErrorResponse) => {
        console.log(JSON.stringify(err.error))
        this.CustomerPoint = {
          CustomerTransactionHistory_Amount : "Http Error Response",
          CustomerTransactionHistory_RedeemPoint : "Http Error Response",
          CustomerTransactionHistory_LuckyDrawPoint : "Http Error Response",
          CustomerTransactionHistory_TotalAmount : "Http Error Response",
          CustomerRedeemPoint_TotalRedeemPoint : "Http Error Response",
          CustomerRedeemPoint_TotalLuckyDrawPoint : "Http Error Response"
        }
      });
  }

}
