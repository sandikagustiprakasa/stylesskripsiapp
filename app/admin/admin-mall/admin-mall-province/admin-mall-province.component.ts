import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import * as Dialog from 'ui/dialogs';

import { Color } from "tns-core-modules/ui/core/view"

import { GlobalParamSite } from '../../../../shared/service-proxy.service';
import { ProvinceService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';


@Component({
  selector: 'app-admin-mall-province',
  templateUrl: './admin-mall-province.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-mall-province.component.css'
  ]
})
export class AdminMallProvinceComponent implements OnInit {

 //Request Province List
 public ListRequest = {
  MasterProvince_ID : null
}

//Province List Data & Paging 
public ListData = []
public ListDataPaging = {count : 0}

//String variable for information paging
public PagingDisplay = "";

//For abort subscribtion searchProvince List
public Subscribtion = null;

//information display forProvince List
public isProvinceListLoading = false
public isProvinceListEmpty = true
public isPullRefresh = null;

constructor(
  public page: Page,
  private _cRef: ChangeDetectorRef,
  private routerExtensions: RouterExtensions,
  private modalService: ModalDialogService,
  private viewContainerRef: ViewContainerRef,
  private _GlobalParamSite : GlobalParamSite,
  private _ProvinceService :ProvinceService,
  private _SP : ServiceProxy,
) {
  page.actionBarHidden = true;
  setTimeout(function(){ 
    
    page.statusBarStyle = "light";
    page.androidStatusBarBackground = new Color("#F5F5F5");

   }, 500);

   this._SP.setUrlBefore('/admin/mall-filter')
}

ngOnInit() {
  this.GettingListProvinceDataAPI(this.ListRequest,false)
}
//Take By User------------------------------------------------------------------------------------------------
refreshList(args) {
  console.log("Refresh 1")
  this.isPullRefresh = args.object;

  
  this.GettingListProvinceDataAPI(this.ListRequest,true)
  // setTimeout(function () {
  //   this.isPullRefresh .refreshing = false;
  //   console.log("Refresh 2")
  // }, 1000);
}

BackToPageBefore(){
  this.routerExtensions.navigate(['/admin/mall-filter'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}

onItemTap(args: ItemEventData){
  //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);
  this._GlobalParamSite.SetProvince({
    ID : this.ListData[args.index].MasterProvince_ID,
    Name : this.ListData[args.index].MasterProvince_Name
  })
  this.routerExtensions.navigate(['/admin/mall-filter'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}


BackAfterListEmpty(){
  this.GettingListProvinceDataAPI(this.ListRequest,false)
}



//Funtion Engine to API---------------------------------------------------------------------------------------
GettingListProvinceDataAPI(_Request,_isPull){
  this.ListData = []
  this.isProvinceListLoading = true
  this.isProvinceListEmpty = false
  this.Subscribtion = this._ProvinceService.List(_Request)
    .subscribe((result: any) => {
      if(result != null){
        //console.log(JSON.stringify(result))

        if(result.Data != null){
          
          this.ListData = result.Data

        }else{
          this.ListData = []
          this.isProvinceListEmpty = true
        }
      }else{
        this.ListData = []
        this.isProvinceListEmpty = true
      }
      
      this.isProvinceListLoading = false

      if(_isPull == true){
        this.isPullRefresh.refreshing = false;
      }

      this._cRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
      console.log("Error!", JSON.stringify(err.error))

      const options : Dialog.AlertOptions = {
        title : "OH!",
        message : JSON.stringify(err.error),
        okButtonText : 'Okay'
      }
      Dialog.alert(options).then(()=>{
      })
      
      this.isProvinceListLoading = false
      this.isProvinceListEmpty = true

      this._cRef.detectChanges();
    });
}

}
