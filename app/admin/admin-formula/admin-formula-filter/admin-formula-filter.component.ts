import { Component, OnInit} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"
import { EventData } from "tns-core-modules/data/observable";
import { View, Color } from "tns-core-modules/ui/core/view"
import { Switch } from "tns-core-modules/ui/switch";

import { GlobalParamFormula } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-formula-filter',
  templateUrl: './admin-formula-filter.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-formula-filter.component.css'
  ]
})
export class AdminFormulaFilterComponent implements OnInit {

  public PointTypeFilter = {
    ID : null,
    Name : ""
  };

  public IsDefaultFilter = [
    {
      ID : null,
      Name : "Choose All",
      Active : false
    },
    {
      ID : true,
      Name : "No Periode Formula",
      Active : false
    },
    {
      ID : false,
      Name : "Periode Formula",
      Active : false
    }
  ];
  public IsDefaultFilterSingle = null

  public IsSpecialFilter = [
    {
      ID : null,
      Name : "Choose All",
      Active : false
    },
    {
      ID : false,
      Name : "Non Special Formula",
      Active : false
    },
    {
      ID : true,
      Name : "Special Formula",
      Active : false
    }
  ];
  public IsSpecialFilterSingle = null

  public IsActiveMsg = ""
  public IsActiveFilterSingle = null


  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _GlobalParamFormula : GlobalParamFormula,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this.PointTypeFilter = this._GlobalParamFormula.GetPointType
    var th = this
    this.IsDefaultFilter.map(function(x, index){
      if(th._GlobalParamFormula.GetIsDefault == x.ID){
        x.Active = true
        th.IsDefaultFilterSingle = x.ID
      }else{
        x.Active = false
      }
    })
    this.IsSpecialFilter.map(function(x, index){
      if(th._GlobalParamFormula.GetIsSpecial == x.ID){
        x.Active = true
        th.IsSpecialFilterSingle = x.ID
      }else{
        x.Active = false
      }
    })

    this._SP.setUrlBefore('/admin/formula')

  }
  //Mobile Code ------------------------------------------------------------------------------------------------
  public wrapperLayout : View = undefined;
  public SwitchActive : Switch = undefined

  onWrapperLoaded(args : EventData){
    this.wrapperLayout = args.object as View;
    const page = this.wrapperLayout.page;

    this.SwitchActive = page.getViewById("ActiveSwitch") as Switch;
    this.SwitchActive.checked = this._GlobalParamFormula.GetIsActive;

    this.IsActiveMsg = this._GlobalParamFormula.GetIsActive ? 'Active' : 'Inactive';
    this.IsActiveFilterSingle = this._GlobalParamFormula.GetIsActive;
  }

  //Take By User------------------------------------------------------------------------------------------------
 
  onCheckedChange(args: EventData) {
    let sw = args.object as Switch;
    this.SwitchActive.checked = sw.checked; // boolean
    this._GlobalParamFormula.SetIsActive(sw.checked);

    this.IsActiveMsg = this._GlobalParamFormula.GetIsActive ? 'Active' : 'Inactive';
    this.IsActiveFilterSingle = this._GlobalParamFormula.GetIsActive;
  }

  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/formula'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoFilter(){
    this.routerExtensions.navigate(['/admin/formula'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToPointTypePage(){

		this.routerExtensions.navigate(['/admin/formula-pointtype'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }


  TimesClick(_Filter){
    if(_Filter == 'PointType'){
      this.PointTypeFilter.ID = null
      this.PointTypeFilter.Name = ""
      this._GlobalParamFormula.SetPointType(this.PointTypeFilter)
    }
  }

  ChooseTimingFormula(_i){
    this.IsDefaultFilter.map(function(x, index){
      x.Active = false
    })
    this.IsDefaultFilter[_i].Active = true
    this.IsDefaultFilterSingle = this.IsDefaultFilter[_i].ID
    this._GlobalParamFormula.SetIsDefault(this.IsDefaultFilter[_i].ID)
  }

  ChooseSpecialormula(_i){
    this.IsSpecialFilter.map(function(x, index){
      x.Active = false
    })
    this.IsSpecialFilter[_i].Active = true
    this.IsSpecialFilterSingle = this.IsSpecialFilter[_i].ID
    this._GlobalParamFormula.SetIsSpecial(this.IsSpecialFilter[_i].ID)
  }
}
