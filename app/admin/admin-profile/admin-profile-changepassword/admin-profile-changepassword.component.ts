import { Component, OnInit} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"
import { EventData } from "tns-core-modules/data/observable";
import { View, Color } from "tns-core-modules/ui/core/view"
import { Switch } from "tns-core-modules/ui/switch";

import * as ImageSourceModule from "tns-core-modules/image-source/";

import * as Dialog from 'ui/dialogs';

import { AuthService } from '../../../../shared/service-proxy.service';
import { GlobalParamChangePassowrd } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

import { HttpErrorResponse } from '@angular/common/http';

const AratakaConfig = require("~/assets/AratakaConfig.js").AratakaConfig;

@Component({
  selector: 'app-admin-profile-changepassword',
  templateUrl: './admin-profile-changepassword.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-profile-changepassword.component.css'
  ]
})
export class AdminProfileChangepasswordComponent implements OnInit {
  
  public APIURL = this._SP.APIURL;

  public OldPassword : string = ""
  public NewPassword : string = ""
  public RepeatPassword : string = ""

  constructor(
    public page: Page,
    private routerExtensions: RouterExtensions,
    private _AuthService : AuthService,
    private _GlobalParamChangePassowrd : GlobalParamChangePassowrd,
    private _SP : ServiceProxy
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this._SP.setUrlBefore('/admin/profile')
  }

  //Take Form User--------------------------------------------------------
  BackToPageBefore(){
		this.routerExtensions.navigate(['/admin/profile'],
				{
					clearHistory: true,
					animated: true,
					transition: {
						name: "slideRight",
						duration: 300,
						curve: "easeIn"
					}
				}
			);
    }
    
  SubmitUpdatePassword(){
    
    var formValidation = {
      isValid : false,
      message : ""
    }
    const options : Dialog.AlertOptions = {
      title : "Oh!",
      message : "",
      okButtonText : 'Understand'
    }
  
    

    if(this.OldPassword == null || this.OldPassword == ""){
      formValidation.isValid = false
      formValidation.message = "Please fill empty field"
      
      options.message = formValidation.message
      Dialog.alert(options).then(()=>{
      })

      return false
    }
    if(this.NewPassword == null || this.NewPassword == ""){
      formValidation.isValid = false
      formValidation.message = "Please fill empty field"
      
      options.message = formValidation.message
      Dialog.alert(options).then(()=>{
      })

      return false
    }
    if(this.RepeatPassword == null || this.RepeatPassword == ""){
      formValidation.isValid = false
      formValidation.message = "Please fill empty field"
      
      options.message = formValidation.message
      Dialog.alert(options).then(()=>{
      })

      return false
    }


    if(!this.strongPassword(this.NewPassword)){

      formValidation.isValid = false
      formValidation.message = "Password doesn't meet minimum complexity policy"
      
      options.message = formValidation.message
      Dialog.alert(options).then(()=>{
      })

      return false
    }

    if(this.NewPassword != this.RepeatPassword){
      formValidation.isValid = false
      formValidation.message = "Miss match password"
      
      options.message = formValidation.message
      Dialog.alert(options).then(()=>{
      })

      return false
    }


    var TransactionRequest = {
      MasterUserData_ID: this._SP.getUserInformation.Data.MasterUserData_ID,
      OldPassword:  AratakaConfig.Sha256(this.OldPassword),
      Password: AratakaConfig.Sha256(this.NewPassword),
      DobleCheck: true,
      EncryptedText: null
    }
    console.log("Req ", JSON.stringify(TransactionRequest))
    this.UpdatePasswordAPI(TransactionRequest)

  }

  strongPassword(password) {
    
    var regExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{8,}/;
    var validPassword = regExp.test(password);
    return validPassword;

  }

  //API Function --------------------------------------------------------------------
  UpdatePasswordAPI(_Request){
		this._AuthService.UpdatePassword(_Request)
		  .subscribe((result: any) => {
      console.log("Trans",JSON.stringify(result));
      
      this._GlobalParamChangePassowrd.SetIsTransactionSuccess(false)
      this._GlobalParamChangePassowrd.SetTransactiomMsg(result.Message)

			if (result.Code == 1) {
          this._GlobalParamChangePassowrd.SetIsTransactionSuccess(true)

			}
      
        this.routerExtensions.navigate(['/admin/profile-changepassword/notif/'],
        {
            clearHistory: true,
            animated: true,
            transition: {
              name: "slideRight",
              duration: 300,
              curve: "easeIn"
            }
          }
        );

		  },
		  (err: HttpErrorResponse) => {
        console.log(JSON.stringify(err));
        alert(JSON.stringify(err))
		  });
	
  }
  


}
