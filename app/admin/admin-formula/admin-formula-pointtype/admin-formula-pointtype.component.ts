import { Component, OnInit, ViewContainerRef, ChangeDetectorRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import * as Dialog from 'ui/dialogs';

import { Color } from "tns-core-modules/ui/core/view"

import { GlobalParamFormula } from '../../../../shared/service-proxy.service';
import {PointTypeService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';


@Component({
  selector: 'app-admin-formula-pointtype',
  templateUrl: './admin-formula-pointtype.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-formula-pointtype.component.css'
  ]
})
export class AdminFormulaPointtypeComponent implements OnInit {

  //Request PointType List
  public ListRequest = {
    MasterPointType_ID : null
  }

  //PointType List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchPointType List
  public Subscribtion = null;

  //information display forPointType List
  public isPointTypeListLoading = false
  public isPointTypeListEmpty = true
  public isPullRefresh = null;

  constructor(
    public page: Page,
    private _cRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private modalService: ModalDialogService,
    private viewContainerRef: ViewContainerRef,
    private _GlobalParamFormula : GlobalParamFormula,
    private _PointTypeService :PointTypeService,
    private _SP : ServiceProxy,
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {
    this.GettingListPointTypeDataAPI(this.ListRequest,false)

    this._SP.setUrlBefore('/admin/formula-filter')
  }
  //Take By User------------------------------------------------------------------------------------------------
  refreshList(args) {
    console.log("Refresh 1")
    this.isPullRefresh = args.object;

    
    this.GettingListPointTypeDataAPI(this.ListRequest,true)
    // setTimeout(function () {
    //   this.isPullRefresh .refreshing = false;
    //   console.log("Refresh 2")
    // }, 1000);
  }

  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/formula-filter'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }

  onItemTap(args: ItemEventData){
    //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);
    this._GlobalParamFormula.SetPointType({
      ID : this.ListData[args.index].MasterPointType_ID,
      Name : this.ListData[args.index].MasterPointType_Name
    })
    this.routerExtensions.navigate(['/admin/formula-filter'],
      {
        clearHistory: true,
        animated: true,
        transition: {
          name: "slideRight",
          duration: 300,
          curve: "easeIn"
        }
      }
    );
  }


  BackAfterListEmpty(){
    this.GettingListPointTypeDataAPI(this.ListRequest,false)
  }



  //Funtion Engine to API---------------------------------------------------------------------------------------
  GettingListPointTypeDataAPI(_Request,_isPull){
    this.ListData = []
    this.isPointTypeListLoading = true
    this.isPointTypeListEmpty = false
    this.Subscribtion = this._PointTypeService.List(_Request)
      .subscribe((result: any) => {
        if(result != null){
          //console.log(JSON.stringify(result))

          if(result.Data != null){
            
            this.ListData = result.Data

          }else{
            this.ListData = []
            this.isPointTypeListEmpty = true
          }
        }else{
          this.ListData = []
          this.isPointTypeListEmpty = true
        }
        
        this.isPointTypeListLoading = false

        if(_isPull == true){
          this.isPullRefresh.refreshing = false;
        }

        this._cRef.detectChanges();
      },
      (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
          title : "OH!",
          message : JSON.stringify(err.error),
          okButtonText : 'Okay'
        }
        Dialog.alert(options).then(()=>{
        })
        
        this.isPointTypeListLoading = false
        this.isPointTypeListEmpty = true

        this._cRef.detectChanges();
      });
  }

}
