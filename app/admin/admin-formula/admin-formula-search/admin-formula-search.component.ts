import { Component, OnInit, ViewContainerRef, ChangeDetectorRef, ViewChildren, ElementRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ItemEventData } from "tns-core-modules/ui/list-view";
import { RouterExtensions } from "nativescript-angular/router";
import { HttpErrorResponse } from '@angular/common/http';
import { Page } from "ui/page"
import { TextField } from "tns-core-modules/ui/text-field"
import * as Dialog from 'ui/dialogs';

import { View, Color } from "tns-core-modules/ui/core/view"
import { EventData } from "tns-core-modules/data/observable";

import { GlobalParamFormula } from '../../../../shared/service-proxy.service';
import { FormulaService } from '../../../../shared/service-proxy.service';
import { ServiceProxy } from '../../../../shared/service-proxy.service';

@Component({
  selector: 'app-admin-formula-search',
  templateUrl: './admin-formula-search.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-formula-search.component.css'
  ]
})
export class AdminFormulaSearchComponent implements OnInit {

  //Filter model
  public keyword : string = ""

  //RequestFormula List
  public ListRequest = {
    Page: 1,
    PerPage: 5,
    Search : "",
    IsDefault: null,
    IsSpecial: null,
    IsActive: true,
    MasterFormulaPoint_ID: null,
    MasterPointType_ID: null,
    MasterProject_ID: this._SP.ProjectID
  }

  //Formula List Data & Paging 
  public ListData = []
  public ListDataPaging = {count : 0}
  
  //String variable for information paging
  public PagingDisplay = "";

  //For abort subscribtion searchFormula List
  public Subscribtion = null;

  //information display forFormula List
  public isFormulaListLoading = false
  public isFormulaListEmpty = true

constructor(
  public page: Page,
  private _cRef: ChangeDetectorRef,
  private routerExtensions: RouterExtensions,
  private modalService: ModalDialogService,
  private viewContainerRef: ViewContainerRef,
  private _GlobalParamFormula : GlobalParamFormula,
  private _FormulaService :FormulaService,
  private _SP : ServiceProxy,
) {
  page.actionBarHidden = true;
  setTimeout(function(){ 
      
    page.statusBarStyle = "light";
    page.androidStatusBarBackground = new Color("#F5F5F5");

   }, 500);
}

ngOnInit() {
   
  this.keyword = this._GlobalParamFormula.GetSearch

  this.GettingListFormulaDataAPI(this.ListRequest)

  this._SP.setUrlBefore('/admin/formula')
  
}

//Mobile Code ------------------------------------------------------------------------------------------------
public wrapperLayout : View = undefined;
public searchBar : TextField = undefined;


onWrapperLoaded(args : EventData){
      
  this.wrapperLayout = args.object as View;
  const page = this.wrapperLayout.page;

  this.searchBar = page.getViewById("searchBar") as TextField;

  this.searchBar.focus();
}

//Take By User------------------------------------------------------------------------------------------------

BackToPageBefore(){
  this.routerExtensions.navigate(['/admin/formula'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}

onItemTap(args: ItemEventData){
  //console.log(`Index: ${args.index}; View: ${args.view} ; Item: ${this.items[args.index]}`);

  this._GlobalParamFormula.SetSearch(this.ListData[args.index].MasterFormulaPoint_Name)

  this.routerExtensions.navigate(['/admin/formula'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}


KeywordReturnPress(){
  this.ListRequest.Search = this.keyword      
  this.DoSearch()
}

KeywordChange(){
  
  this.ListRequest.Search = this.keyword      
  this.DoSearch()

}

DoSearch(){
    
  this.ListRequest.Page = 1
    if(this.Subscribtion){
    this.Subscribtion.unsubscribe()
  }
  this.GettingListFormulaDataAPI(this.ListRequest)    

}

BackAfterListEmpty(){
  this.keyword = ""
  this.ListRequest.Search = this.keyword    
  this.GettingListFormulaDataAPI(this.ListRequest)
}

TimesClick(){
  this._GlobalParamFormula.SetSearch("")
  this.routerExtensions.navigate(['/admin/formula'],
    {
      clearHistory: true,
      animated: true,
      transition: {
        name: "slideRight",
        duration: 300,
        curve: "easeIn"
      }
    }
  );
}


//Funtion Engine to API---------------------------------------------------------------------------------------
GettingListFormulaDataAPI(_Request){
  this.ListData = []
  this.isFormulaListLoading = true
  this.isFormulaListEmpty = false
  this.Subscribtion = this._FormulaService.List(_Request)
    .subscribe((result: any) => {
      if(result != null){
        //console.log(JSON.stringify(result))

        if(result.Data != null){
          
          this.ListData = result.Data
          this.ListDataPaging.count = result.TotalData

        }else{
          this.ListData = []
          this.isFormulaListEmpty = true
        }
      }else{
        this.ListData = []
        this.isFormulaListEmpty = true
      }
      
      this.isFormulaListLoading = false

      this._cRef.detectChanges();
    },
    (err: HttpErrorResponse) => {
      console.log("Error!", JSON.stringify(err.error))

      const options : Dialog.AlertOptions = {
        title : "OH!",
        message : JSON.stringify(err.error),
        okButtonText : 'Okay'
      }
      Dialog.alert(options).then(()=>{
      })
      
      this.isFormulaListLoading = false
      this.isFormulaListEmpty = true

      this._cRef.detectChanges();
    });
  }
}
