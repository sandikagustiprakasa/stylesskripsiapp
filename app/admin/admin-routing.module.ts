import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { AdminFirstPageComponent } from "./admin-first-page/admin-first-page.component";
import { AdminHomeComponent } from "./admin-home/admin-home.component";
import { AdminTestprintComponent } from './admin-testprint/admin-testprint.component';
import { AdminMallComponent } from './admin-mall/admin-mall.component';
import { AdminMallFilterComponent } from './admin-mall/admin-mall-filter/admin-mall-filter.component';
import { AdminMallProvinceComponent } from './admin-mall/admin-mall-province/admin-mall-province.component';
import { AdminMallSearchComponent } from './admin-mall/admin-mall-search/admin-mall-search.component';
import { AdminBrandComponent } from './admin-brand/admin-brand.component';
import { AdminBrandFilterComponent } from './admin-brand/admin-brand-filter/admin-brand-filter.component';
import { AdminBrandSearchComponent } from './admin-brand/admin-brand-search/admin-brand-search.component';
import { AdminBrandStorecategoryComponent } from './admin-brand/admin-brand-storecategory/admin-brand-storecategory.component';
import { AdminMerchantComponent } from './admin-merchant/admin-merchant.component';
import { AdminMerchantFilterComponent } from './admin-merchant/admin-merchant-filter/admin-merchant-filter.component';
import { AdminMerchantSearchComponent } from './admin-merchant/admin-merchant-search/admin-merchant-search.component';
import { AdminMerchantStorecategoryComponent } from './admin-merchant/admin-merchant-storecategory/admin-merchant-storecategory.component';
import { AdminMerchantBrandComponent } from './admin-merchant/admin-merchant-brand/admin-merchant-brand.component';
import { AdminCardtypeComponent } from './admin-cardtype/admin-cardtype.component';
import { AdminCardtypeSearchComponent } from './admin-cardtype/admin-cardtype-search/admin-cardtype-search.component';
import { AdminFormulaComponent } from './admin-formula/admin-formula.component';
import { AdminFormulaDetailComponent } from './admin-formula/admin-formula-detail/admin-formula-detail.component';
import { AdminFormulaSearchComponent } from './admin-formula/admin-formula-search/admin-formula-search.component';
import { AdminFormulaFilterComponent } from './admin-formula/admin-formula-filter/admin-formula-filter.component';
import { AdminFormulaPointtypeComponent } from './admin-formula/admin-formula-pointtype/admin-formula-pointtype.component';
import { AdminRedeemComponent } from './admin-redeem/admin-redeem.component';
import { AdminRedeemSearchComponent } from './admin-redeem/admin-redeem-search/admin-redeem-search.component';
import { AdminRedeemDetailComponent } from './admin-redeem/admin-redeem-detail/admin-redeem-detail.component';
import { AdminRedeemFilterComponent } from './admin-redeem/admin-redeem-filter/admin-redeem-filter.component';
import { AdminRedeemStorecategoryComponent } from './admin-redeem/admin-redeem-storecategory/admin-redeem-storecategory.component';
import { AdminCustomerComponent } from './admin-customer/admin-customer.component';
import { AdminCustomerSearchComponent } from './admin-customer/admin-customer-search/admin-customer-search.component';
import { AdminCustomerDetailComponent } from './admin-customer/admin-customer-detail/admin-customer-detail.component';
import { AdminCustomerTransactionComponent } from './admin-customer/admin-customer-transaction/admin-customer-transaction.component';
import { AdminCustomerTransMerchantComponent } from './admin-customer/admin-customer-trans-merchant/admin-customer-trans-merchant.component';
import { AdminCustomerTransBankComponent } from './admin-customer/admin-customer-trans-bank/admin-customer-trans-bank.component';
import { AdminCustomerTransEwalletComponent } from './admin-customer/admin-customer-trans-ewallet/admin-customer-trans-ewallet.component';
import { AdminCustomerTransHistoryComponent } from './admin-customer/admin-customer-trans-history/admin-customer-trans-history.component';
import { AdminCustomerTransMembershipcardComponent } from './admin-customer/admin-customer-trans-membershipcard/admin-customer-trans-membershipcard.component';
import { AdminCustomerRedeemComponent } from './admin-customer/admin-customer-redeem/admin-customer-redeem.component';
import { AdminCustomerRedeemFilterComponent } from './admin-customer/admin-customer-redeem-filter/admin-customer-redeem-filter.component';
import { AdminCustomerRedeemSearchComponent } from './admin-customer/admin-customer-redeem-search/admin-customer-redeem-search.component';
import { AdminCustomerRedeemStorecategoryComponent } from './admin-customer/admin-customer-redeem-storecategory/admin-customer-redeem-storecategory.component';
import { AdminCustomerRedeemNotiftransComponent } from './admin-customer/admin-customer-redeem/admin-customer-redeem-notiftrans/admin-customer-redeem-notiftrans.component';
import { AdminPrintersettingComponent } from './admin-printersetting/admin-printersetting.component';
import { AdminProfileComponent } from "./admin-profile/admin-profile.component";
import { AdminProfileChangepasswordComponent } from './admin-profile/admin-profile-changepassword/admin-profile-changepassword.component';
import { AdminProfileChangepasswordNotifComponent } from './admin-profile/admin-profile-changepassword/admin-profile-changepassword-notif/admin-profile-changepassword-notif.component';
import { AdminCustomerTransactionNotifComponent } from './admin-customer/admin-customer-transaction/admin-customer-transaction-notif/admin-customer-transaction-notif.component';

const routes: Routes = [
    { path: "", component: AdminFirstPageComponent },
    { path: "mall", component: AdminMallComponent },
    { path: "mall-filter", component: AdminMallFilterComponent },
    { path: "mall-province", component: AdminMallProvinceComponent },
    { path: "mall-search", component: AdminMallSearchComponent },
    { path: "brand", component: AdminBrandComponent },
    { path: "brand-filter", component: AdminBrandFilterComponent },
    { path: "brand-storecategory", component: AdminBrandStorecategoryComponent },
    { path: "brand-search", component: AdminBrandSearchComponent },
    { path: "merchant", component: AdminMerchantComponent },
    { path: "merchant-filter", component: AdminMerchantFilterComponent },
    { path: "merchant-search", component: AdminMerchantSearchComponent },
    { path: "merchant-storecategory", component: AdminMerchantStorecategoryComponent },
    { path: "merchant-brand", component: AdminMerchantBrandComponent },
    { path: "cardtype", component: AdminCardtypeComponent },
    { path: "cardtype-search", component: AdminCardtypeSearchComponent },
    { path: "formula", component: AdminFormulaComponent },
    { path: "formula-filter", component: AdminFormulaFilterComponent },
    { path: "formula-detail", component: AdminFormulaDetailComponent },
    { path: "formula-search", component: AdminFormulaSearchComponent },
    { path: "formula-pointtype", component: AdminFormulaPointtypeComponent },
    { path: "redeem", component: AdminRedeemComponent },
    { path: "redeem-filter", component: AdminRedeemFilterComponent },
    { path: "redeem-detail", component: AdminRedeemDetailComponent },
    { path: "redeem-search", component: AdminRedeemSearchComponent },
    { path: "redeem-storecategory", component: AdminRedeemStorecategoryComponent },
    { path: "customer", component: AdminCustomerComponent },
    { path: "customer-search", component: AdminCustomerSearchComponent },
    { path: "customer-detail", component: AdminCustomerDetailComponent },
    { path: "customer-transaction", component: AdminCustomerTransactionComponent },
    { path: "customer-transaction/notif", component: AdminCustomerTransactionNotifComponent },
    { path: "customer-trans-merchant", component: AdminCustomerTransMerchantComponent },
    { path: "customer-trans-bank", component: AdminCustomerTransBankComponent },
    { path: "customer-trans-ewallet", component: AdminCustomerTransEwalletComponent },
    { path: "customer-trans-history", component: AdminCustomerTransHistoryComponent },
    { path: "customer-trans-membershipcard", component: AdminCustomerTransMembershipcardComponent },
    { path: "customer-redeem", component: AdminCustomerRedeemComponent },
    { path: "customer-redeem-filter", component: AdminCustomerRedeemFilterComponent },
    { path: "customer-redeem-search", component: AdminCustomerRedeemSearchComponent },
    { path: "customer-redeem-storecategory", component: AdminCustomerRedeemStorecategoryComponent },
    { path: "customer-redeem/notiftrans", component: AdminCustomerRedeemNotiftransComponent },
    { path: "printersetting", component: AdminPrintersettingComponent },
    { path: "profile", component: AdminProfileComponent },
    { path: "profile-changepassword", component: AdminProfileChangepasswordComponent },
    { path: "profile-changepassword/notif", component: AdminProfileChangepasswordNotifComponent },
    

    { path: "home", component: AdminHomeComponent },
    { path: "testprint", component: AdminTestprintComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class AdminRoutingModule { }
