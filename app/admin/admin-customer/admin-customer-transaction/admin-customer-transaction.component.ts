import { Component, OnInit} from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"
import { EventData } from "tns-core-modules/data/observable";
import { View, Color } from "tns-core-modules/ui/core/view"
import { Switch } from "tns-core-modules/ui/switch";

import * as ImageSourceModule from "tns-core-modules/image-source/";

import * as Dialog from 'ui/dialogs';

import { GlobalParamTransaction } from '../../../../shared/service-proxy.service';
import { CustomerTransactionHistoryService } from '../../../../shared/service-proxy.service';

import { ServiceProxy } from '../../../../shared/service-proxy.service';
import { GlobalParamCustomerEarning } from '../../../../shared/service-proxy.service';

import { HttpErrorResponse } from '@angular/common/http';

import { Image } from "tns-core-modules/ui/image";
import { FormBuilder, FormGroup  } from '@angular/forms';

const AratakaConfig = require("~/assets/AratakaConfig.js").AratakaConfig;
const atob = require('atob');
import { Blob } from "blob-polyfill";

var LoadingIndicator = require("nativescript-loading-indicator-new").LoadingIndicator;

@Component({
  selector: 'app-admin-customer-transaction',
  templateUrl: './admin-customer-transaction.component.html',
  styleUrls: [
    '../../../assets/local-style.css',
    './admin-customer-transaction.component.css'
  ]
})
export class AdminCustomerTransactionComponent implements OnInit {

  public APIURL = this._SP.APIURL;

  public Receipt : Image = undefined

  public Bank = {
    ID : null,
    Name : ""
  };

  public EWallet = {
    ID : null,
    Name : ""
  };

  public Merchant = {
    ID : null,
    Name : "",
    StoreCategoryID : null,
    StoreCategoryName : ""
  };

  public TransactionType = [
    {
      ID : 1,
      Name : "Cash",
      Active : true
    },
    {
      ID : 2,
      Name : "Debit Card",
      Active : false
    },
    {
      ID : 3,
      Name : "Credit Card",
      Active : false
    },
    {
      ID : 4,
      Name : "E-Wallet",
      Active : false
    }
  ];
  public TransactionTypeSingle = null

  public Amount : number = 0;
  public ReceiptNumber : string = ""

  constructor(
    public page: Page,
    private FormBuilder : FormBuilder, 
    private routerExtensions: RouterExtensions,
    private _GlobalParamTransaction : GlobalParamTransaction,
    private _CustomerTransactionHistoryService : CustomerTransactionHistoryService,
    private _GlobalParamCustomerEarning : GlobalParamCustomerEarning,
    private _SP : ServiceProxy
  ) {
    page.actionBarHidden = true;
    setTimeout(function(){ 
      
      page.statusBarStyle = "light";
      page.androidStatusBarBackground = new Color("#F5F5F5");

     }, 500);
  }

  ngOnInit() {

    this.Receipt = this._GlobalParamTransaction.GetReceipt

    this.Bank = this._GlobalParamTransaction.GetBank
    this.EWallet = this._GlobalParamTransaction.GetEWallet
    this.Merchant = this._GlobalParamTransaction.GetMerchant
    this.Amount = this._GlobalParamTransaction.GetAmount
    this.ReceiptNumber = this._GlobalParamTransaction.GetReceiptNumber

    var th = this;
    this.TransactionType.map(function(x, index){
      if(th._GlobalParamTransaction.GetTransactionID == x.ID){
        x.Active = true
        th.TransactionTypeSingle = x.ID
      }else{
        x.Active = false
      }
    })


    this._SP.setUrlBefore('/admin/customer-detail')
    
  }


  //Take By User------------------------------------------------------------------------------------------------
  BackToPageBefore(){
    this.routerExtensions.navigate(['/admin/customer-detail'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideRight",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToBankPage(){

		this.routerExtensions.navigate(['/admin/customer-trans-bank'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToEWalletPage(){

		this.routerExtensions.navigate(['/admin/customer-trans-ewallet'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  GoToMerchantPage(){

		this.routerExtensions.navigate(['/admin/customer-trans-merchant'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
  }

  TimesClick(_Filter){
    if(_Filter == 'Bank'){
      this.Bank.ID = null
      this.Bank.Name = ""
      this._GlobalParamTransaction.SetBank(this.Bank)
    }

    if(_Filter == 'EWallet'){
      this.EWallet.ID = null
      this.EWallet.Name = ""
      this._GlobalParamTransaction.SetEWallet(this.EWallet)
    }

    if(_Filter == 'Merchant'){
      this.Merchant.ID = null
      this.Merchant.Name = ""
      this.Merchant.StoreCategoryID = null
      this.Merchant.StoreCategoryName = ""
      this._GlobalParamTransaction.SetMerchant(this.Merchant)
    }
  }

  ChooseTransactionType(_i){
    this.TransactionType.map(function(x, index){
      x.Active = false
    })
    this.TransactionType[_i].Active = true
    this.TransactionTypeSingle = this.TransactionType[_i].ID

    this.Bank.ID = null
    this.Bank.Name = ""
    this._GlobalParamTransaction.SetBank(this.Bank)

    this.EWallet.ID = null
    this.EWallet.Name = ""
    this._GlobalParamTransaction.SetEWallet(this.EWallet)

    this._GlobalParamTransaction.SetTransactionID(this.TransactionType[_i].ID)
  }


  SubmitTransaction(){
    
    var dateTimeNow = new Date();
    var formValidation = {
      isValid : false,
      message : ""
    }
    const options : Dialog.AlertOptions = {
      title : "Oh!",
      message : "",
      okButtonText : 'Understand'
    }
	
	 if(this.TransactionTypeSingle == null){
	  formValidation.isValid = false
	  formValidation.message = "Please select transaction type"
	  
	  options.message = formValidation.message
	  Dialog.alert(options).then(()=>{
	  })

	  return false
	 }
    
    if(this.TransactionTypeSingle != 1){
      if(this.TransactionTypeSingle == 2 || this.TransactionTypeSingle == 3){
        if(this.Bank.ID == null){
          formValidation.isValid = false
          formValidation.message = "Please select Bank"
          
          options.message = formValidation.message
          Dialog.alert(options).then(()=>{
          })

          return false
        }
      }
      
      if(this.TransactionTypeSingle == 4){
        if(this.EWallet.ID == null){
          formValidation.isValid = false
          formValidation.message = "Please select E-Wallet"
          
          options.message = formValidation.message
          Dialog.alert(options).then(()=>{
          })

          return false
        }
      }
    }

    if(this.Merchant.ID == null){
      formValidation.isValid = false
      formValidation.message = "Please select Merchant"
      
      options.message = formValidation.message
      Dialog.alert(options).then(()=>{
      })

      return false
    }

    if(this.Amount == null || this.Amount == 0){
      formValidation.isValid = false
      formValidation.message = "Please input Amount"
      
      options.message = formValidation.message
      Dialog.alert(options).then(()=>{
      })

      return false
    }



    var TransactionRequest = {
      MasterMemberCardType_ID : this._GlobalParamTransaction.GetCardType.ID,
      CustomerDataMemberCardType_ID: this._GlobalParamTransaction.GetCardType.CustomerDataMemberCardType_ID,
      MasterEarningType_ID: 1,
      MasterCustomerData_ID: this._GlobalParamTransaction.GetCustomerID,
      MasterTransactionType_ID: this.TransactionTypeSingle,
      MasterBank_ID: this.Bank.ID,
      MasterEWallet_ID: this.EWallet.ID,
      MasterMerchant_ID: this.Merchant.ID,
      MasterStoreCategory_ID: this.Merchant.StoreCategoryID,
      MasterRedeem_ID : null,
      MasterSite_ID: this._SP.UserSiteData.ID,
      MasterProject_ID: this._SP.ProjectID,
      StrukPicture: null,
      TransactionDate: dateTimeNow,
      Amount: this.Amount,
      RedeemPoint : null,
      LuckyDrawPoint : null,
      ReceiptNumber: this.ReceiptNumber,
      Note : null,
      IsConfirmation: 1
    }
    console.log(JSON.stringify(TransactionRequest))

    this.TransactionHistoryInsert(TransactionRequest)

  }

  //API Function --------------------------------------------------------------------
  TransactionHistoryInsert(_Request){
    var loader = new LoadingIndicator();
    var optionsLoading = {
      message: 'Earning Transaction..',
      progress: 0.65,
      android: {
        indeterminate: true,
        cancelable: false,
        max: 100,
        progressNumberFormat: "%1d/%2d",
        progressPercentFormat: 0.53,
        progressStyle: 1,
        secondaryProgress: 1
      },
    };

    loader.show(optionsLoading); // options is optional  

		this._CustomerTransactionHistoryService.Insert(_Request)
		  .subscribe((result: any) => {
      console.log("Trans",JSON.stringify(result));
      
      this._GlobalParamCustomerEarning.SetIsTransactionSuccess(false)
      this._GlobalParamCustomerEarning.SetTransactiomMsg(result.Message)
      this._GlobalParamCustomerEarning.SetCustomerTransactionHistoryID(null)

      if(result.Code != 0){
        
        ImageSourceModule.fromAsset(this.Receipt.src).then(image => { 
          let base64 = image.toBase64String('png', 25); 

          this.UpdateStrukPicture({
            CustomerTransactionHistory_ID : result.CustomerTransactionHistory_ID,
            StrukPicture : base64
          })
          
        })

        this._GlobalParamCustomerEarning.SetIsTransactionSuccess(true)
        if(result.CustomerTransactionHistory_ID != 0){
          
          this._GlobalParamCustomerEarning.SetCustomerTransactionHistoryID(result.CustomerTransactionHistory_ID)
        }
      }

        this.routerExtensions.navigate(['/admin/customer-transaction/notif'],
        {
          clearHistory: true,
          animated: true,
          transition: {
            name: "slideLeft",
            duration: 300,
            curve: "easeIn"
          }
        }
      );

      loader.hide();
    
		  },
		  (err: HttpErrorResponse) => {
        console.log("Error!", JSON.stringify(err.error))

        const options : Dialog.AlertOptions = {
					title : "OH!",
					message : JSON.stringify(err.error),
					okButtonText : 'Okay'
				}
				Dialog.alert(options).then(()=>{
        })
        
        loader.hide();

		  });
	
  }
  
  UpdateStrukPicture(_Request){
    this._CustomerTransactionHistoryService.StrukUpdate(_Request)
      .subscribe((result: any) => {
        console.log("Pict", JSON.stringify(result));
      },
      (err: HttpErrorResponse) => {
        console.log(JSON.stringify(err));
        alert(JSON.stringify(err))
      });
  }
	

}
