import { Component, OnInit, ViewContainerRef } from "@angular/core";
import {setCurrentOrientation , orientationCleanup} from 'nativescript-screen-orientation';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page"
import { View, Color } from "tns-core-modules/ui/core/view"
import * as ApplicationSettings from "application-settings";
import * as Dialog from 'ui/dialogs';

import { ServiceProxy } from '../../../shared/service-proxy.service';

import { AdminFirstPageSwitchsiteComponent } from "./admin-first-page-switchsite/admin-first-page-switchsite.component";
import { GlobalParamPrinterSetting } from '../../../shared/service-proxy.service';


@Component({
	selector: "AdminFirstPage",
	moduleId: module.id,
	templateUrl: "./admin-first-page.component.html",
	styleUrls: [
		'../../assets/local-style.css',
		'./admin-first-page.component.css',
	]
})
export class AdminFirstPageComponent implements OnInit {

	public APIURL = this._SP.APIURL;
	public Sites = [];

	public CustomerServiceGenderCall : string = null
	public CustomerServiceName : string = null
	public CustomerServiceJob : string = null
	public CustomerServiceSites = {
		ID : null,
		Name : null,
		Picture : null
	}

	constructor(
		page: Page,
		private routerExtensions: RouterExtensions,
		private modalService: ModalDialogService,
		private viewContainerRef: ViewContainerRef,
		private _GlobalParamPrinterSetting : GlobalParamPrinterSetting,
		private _SP : ServiceProxy,
	) {
		page.actionBarHidden = true;
		page.statusBarStyle = "dark";
		page.androidStatusBarBackground = new Color("#004D40");
	}

	ngOnInit(): void {
		let that = this
		setTimeout(() => {
			that. CustomerServiceName = that._SP.getUserInformation.Data.MasterUserData_Name;
			that.CustomerServiceJob = that._SP.getUserInformation.Data.MasterJobPosition_Name;
			that.CustomerServiceGenderCall = (that._SP.getUserInformation.Data.MasterUserData_Gender == 1)? "Mr." : "Mrs."; 
				
	
			that.CustomerServiceSites.ID =  that._SP.UserSiteData.ID;
			that.CustomerServiceSites.Name =  that._SP.UserSiteData.Name;
			that.CustomerServiceSites.Picture =  that._SP.UserSiteData.Picture;
	
			setCurrentOrientation("portrait",function(){
				console.log("portrait orientation");
			});
	
			that.Sites = JSON.parse(ApplicationSettings.getString('StorageUserLog')).AppSite
			
		}, 500);


		//console.log(JSON.stringify(JSON.parse(ApplicationSettings.getString('StorageUserLog')).AppSite[1]))
	}

	//Take By User ------------------------------------------------------------------------------------------


	GoProfile(){
		this.routerExtensions.navigate(['/admin/profile'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);

	}

	SwitchSiteShowModal() {
		const options: ModalDialogOptions = {
			viewContainerRef: this.viewContainerRef,
			fullscreen: false,
			animated : true,
			context: {}
		};
		this.modalService.showModal(AdminFirstPageSwitchsiteComponent, options).then(result => {
			// result value is the string you attach in closeCallBack(‘cute’)
			this._SP.UserSiteData.ID = result.ID
			this._SP.UserSiteData.Name =  result.Name
			this._SP.UserSiteData.Picture =  result.Picture

			this.CustomerServiceSites.ID =  this._SP.UserSiteData.ID;
			this.CustomerServiceSites.Name =  this._SP.UserSiteData.Name;
			this.CustomerServiceSites.Picture =  this._SP.UserSiteData.Picture;
	
		})
		.catch(error => {
			// const options : Dialog.AlertOptions = {
			// 	title : "OH Error!",
			// 	message : "No Response",
			// 	okButtonText : 'Okay'
			// }
			// Dialog.alert(options).then(()=>{
			// })
		});

	}

	GoToMallPage(){

		this.routerExtensions.navigate(['/admin/mall'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}

	GoToBrandPage(){
		this.routerExtensions.navigate(['/admin/brand'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}

	GoToMerchantPage(){
		this.routerExtensions.navigate(['/admin/merchant'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}

	GoToCardTypePage(){
		this.routerExtensions.navigate(['/admin/cardtype'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}

	GoToCustomerPage(){
		this.routerExtensions.navigate(['/admin/customer'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}

	GoToFormulaPage(){
		this.routerExtensions.navigate(['/admin/formula'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}

	GoToRedeemPage(){
		this.routerExtensions.navigate(['/admin/redeem'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}

	GoToPrinterSettingPage(){
		this._GlobalParamPrinterSetting.SetActionFrom("")
		this.routerExtensions.navigate(['/admin/printersetting'],
			{
				clearHistory: true,
				animated: true,
				transition: {
					name: "slideLeft",
					duration: 300,
					curve: "easeIn"
				}
			}
		);
	}


	
}